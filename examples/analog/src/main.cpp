#include <wit/wit.h>
#include <wit/application.h>
#include <wit/eventhandler.h>
#include <wit/renderer.h>
#include <wit/event.h>
#include <wit/font.h>
#include <string>

wit::Font* font;

class Meter1D : public wit::Renderer, public wit::EventHandler {
public:
    std::string label;
    wit::Rect rect;
    int button;
    double amount; // between 0.0 and 1.0

    Meter1D(int button, wit::Rect rect) : wit::Renderer(), wit::EventHandler(), rect(rect), button(button), amount(0.0) { }

    void render() {
        wit::Rect fill = rect;
        fill.y1 = fill.y2 - (fill.y2 - fill.y1) * amount;
        fillRect(fill, (GXColor){ 255, 0, 0, 255 });
        drawRect(rect, (GXColor){ 255, 255, 255, 255 });
        font->draw(rect.x1, rect.y2 + 5, label, (GXColor){ 255, 255, 255, 255 });
    }

    bool analogEvent(wit::AnalogEvent* event) {
        if(event->controllerNumber != 0) return true; 
        if(event->trigger != button) return false;
        amount = event->pressure;
        return true;
    }

    bool motionEvent(wit::MotionEvent* event) {
        if(event->controllerNumber != 0) return true;  
        double val = 0;
        if(button >= 0) return false;
        if(button < -3 && event->controllerType == wit::WiiRemote) return false;
        if(button >= -3 && event->controllerType == wit::Nunchuk) return false; 
        if(button == -1 || button == -4) val = event->x;
        else if(button == -2 || button == -5) val = event->y;
        else if(button == -3 || button == -6) val = event->z;
        else return false;

        amount = val; 

        return false;
    }
};

class Meter2D : public wit::Renderer, public wit::EventHandler {
public:
    std::string label;
    wit::Rect rect;
    bool rightStick;
    double x, y; // between 0.0 and 1.0

    Meter2D(bool rightStick, wit::Rect rect) : wit::Renderer(), wit::EventHandler(), rect(rect), rightStick(rightStick), x(0), y(0) {}

    void render() {
        drawRect(rect, (GXColor){ 255, 255, 255, 255 });

        wit::Rect box;
        double w = (rect.x2 - rect.x1 - 5);
        double h = (rect.y2 - rect.y1 - 5);
        box.x1 = rect.x1 + w * ((x / 2.0) + 0.5);
        box.y1 = rect.y1 + h * ((y / 2.0) + 0.5);
        box.x2 = box.x1 + 5;
        box.y2 = box.y1 + 5;
        fillRect(box, (GXColor){ 255, 0, 0, 255 });

        font->draw(rect.x1, rect.y2 + 5, label, (GXColor){ 255, 255, 255, 255 });
    }

    bool joystickEvent(wit::JoystickEvent* event) {
        if(event->controllerType == wit::Nunchuk) {
            if(rightStick) label = "";
            else label = "Nunchuk";
        } else {
            if(rightStick) label = "Right";
            else label = "Left";
        }
        if(event->isRightStick != rightStick) return false;
        x = event->x;
        y = event->y;
        return false;
    }
};

class Pointer : public wit::EventHandler, public wit::Renderer
{
public:
    wit::Polygon cursor;
    float x, y;

    Pointer() : wit::EventHandler(), wit::Renderer() {
        // Here we define the shape of the pointer
        cursor.addPoint(0, 0);
        cursor.addPoint(8, 9);
        cursor.addPoint(4, 9);
        cursor.addPoint(6, 14);
        cursor.addPoint(5, 13);
        cursor.addPoint(3, 9);
        cursor.addPoint(0, 10);
    }

    bool buttonPressEvent(wit::ButtonEvent* event) {
        // As a matter of convenience, the Pointer class also handles 
        // shutting down the program cleanly when the Home button is
        // pressed.
        if(event->button == wit::Button_Home) {
            witApp()->quit();
            // We return "true" here so that no other EventHandler
            // processes the button
            return true;
        }
        // We return "false" here so that any other button presses
        // are not consumed
        return false;
    }

    bool pointerEvent(wit::PointerEvent* event) {
        if(event->controllerNumber != 0) return false;
        x = event->x;
        y = event->y;
        return false;
    }

    void render() {
        // Since the polygon is anchored at (0, 0) we use 
        // setTranslation to move where (0, 0) actually is.
        setTranslation(-x, -y);
        fillPolygon(cursor, (GXColor){ 0, 0, 0, 255 });
        drawPolygon(cursor, (GXColor){ 255, 255, 255, 255 });
    }
};

int main(int argc, char **argv) {
    wit::Application app(wit::UseWiiRemote | wit::UseMotion |
                         wit::UseGX | wit::UseJoysticks |
                         wit::UsePointer);

    wit::Font sans16("Sans16");
    font = &sans16;

    Pointer pointer;
    app.installEventFilter(&pointer);

    Meter1D mLeft(wit::Analog_Left, wit::Rect(40, 20, 60, 120));
    mLeft.label = "L";
    app.installEventFilter(&mLeft);
    Meter1D mRight(wit::Analog_Right, wit::Rect(80, 20, 100, 120));
    mRight.label = "R";
    app.installEventFilter(&mRight);
    Meter1D mA(wit::Analog_A, wit::Rect(120, 20, 140, 120));
    mA.label = "A";
    app.installEventFilter(&mA);
    Meter1D mB(wit::Analog_B, wit::Rect(160, 20, 180, 120));
    mB.label = "B";
    app.installEventFilter(&mB);
    Meter1D mX(-1, wit::Rect(200, 20, 220, 120));
    mX.label = "X";
    app.installEventFilter(&mX);
    Meter1D mY(-2, wit::Rect(240, 20, 260, 120));
    mY.label = "Y";
    app.installEventFilter(&mY);
    Meter1D mZ(-3, wit::Rect(280, 20, 300, 120));
    mZ.label = "Z";
    app.installEventFilter(&mZ);
    Meter1D mX2(-4, wit::Rect(320, 20, 340, 120));
    mX2.label = "N:X";
    app.installEventFilter(&mX2);
    Meter1D mY2(-5, wit::Rect(360, 20, 380, 120));
    mY2.label = "N:Y";
    app.installEventFilter(&mY2);
    Meter1D mZ2(-6, wit::Rect(400, 20, 420, 120));
    mZ2.label = "N:Z";
    app.installEventFilter(&mZ2);

    Meter2D mLeftStick(false, wit::Rect(40, 160, 140, 260));
    mLeftStick.label = "Left";
    app.installEventFilter(&mLeftStick);
    Meter2D mRightStick(true, wit::Rect(260, 160, 360, 260));
    mRightStick.label = "Right";
    app.installEventFilter(&mRightStick);

    app.run();
    return 0;
}
