#include <wit/wit.h>
#include <wit/application.h>
#include <wit/console.h>
#include <wit/eventhandler.h>
#include <wit/event.h>
#include <wit/renderer.h>
#include <wit/widget.h>
#include <stdio.h>

class MatGrid : public wit::EventHandler, public wit::Renderer
{
public:
    bool bUp, bDown, bLeft, bRight, bB, bA, bY, bX, bZ, bStart;

    MatGrid() : wit::EventHandler(), wit::Renderer() {
        bUp = bDown = bLeft = bRight = bB = bA = bY = bX = bZ = bStart = false;
    }

    bool buttonPressEvent(wit::ButtonEvent* event) {
        switch(event->button) {
        case wit::Button_Home:
            witApp()->quit();
            break;
        case wit::Button_Up:
            bUp = true;
            break;
        case wit::Button_Down:
            bDown = true;
            break;
        case wit::Button_Left:
            bLeft = true;
            break;
        case wit::Button_Right:
            bRight = true;
            break;
        case wit::Button_B:
            bB = true;
            break;
        case wit::Button_A:
            bA = true;
            break;
        case wit::Button_X:
            bX = true;
            break;
        case wit::Button_Y:
            bY = true;
            break;
        case wit::Button_Z:
            bZ = true;
            break;
        case wit::Button_Start:
            bStart = true;
            break;
        default:
            return false;
        }
        return true;
    }

    bool buttonReleaseEvent(wit::ButtonEvent* event) {
        switch(event->button) {
        case wit::Button_Up:
            bUp = false;
            break;
        case wit::Button_Down:
            bDown = false;
            break;
        case wit::Button_Left:
            bLeft = false;
            break;
        case wit::Button_Right:
            bRight = false;
            break;
        case wit::Button_B:
            bB = false;
            break;
        case wit::Button_A:
            bA = false;
            break;
        case wit::Button_X:
            bX = false;
            break;
        case wit::Button_Y:
            bY = false;
            break;
        case wit::Button_Z:
            bZ = false;
            break;
        case wit::Button_Start:
            bStart = false;
            break;
        default:
            return false;
        }
        return true;
    }

    void render() {
        bool bNone = !(bLeft | bRight | bUp | bDown);

        fillRect(wit::Rect(0, 0, 639, 479), (GXColor){ 0, 0, 0, 255 });

        drawRect(wit::Rect(0, 0, 319, 59), (GXColor){ 255, 255, 255, 255 });
        if(bZ) fillRect(wit::Rect(0, 0, 319, 59), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(319, 0, 639, 59), (GXColor){ 255, 255, 255, 255 });
        if(bStart) fillRect(wit::Rect(319, 0, 639, 59), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(0, 59, 213, 199), (GXColor){ 255, 255, 255, 255 });
        if(bB) fillRect(wit::Rect(0, 59, 213, 199), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(213, 59, 426, 199), (GXColor){ 255, 255, 255, 255 });
        if(bUp) fillRect(wit::Rect(213, 59, 426, 199), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(426, 59, 639, 199), (GXColor){ 255, 255, 255, 255 });
        if(bA) fillRect(wit::Rect(426, 59, 639, 199), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(0, 199, 213, 339), (GXColor){ 255, 255, 255, 255 });
        if(bLeft) fillRect(wit::Rect(0, 199, 213, 339), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(213, 199, 426, 339), (GXColor){ 255, 255, 255, 255 });
        if(bNone) fillRect(wit::Rect(213, 199, 426, 339), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(426, 199, 639, 339), (GXColor){ 255, 255, 255, 255 });
        if(bRight) fillRect(wit::Rect(426, 199, 639, 339), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(0, 339, 213, 479), (GXColor){ 255, 255, 255, 255 });
        if(bY) fillRect(wit::Rect(0, 339, 213, 479), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(213, 339, 426, 479), (GXColor){ 255, 255, 255, 255 });
        if(bDown) fillRect(wit::Rect(213, 339, 426, 479), (GXColor){ 255, 255, 255, 255 });

        drawRect(wit::Rect(426, 339, 639, 479), (GXColor){ 255, 255, 255, 255 });
        if(bX) fillRect(wit::Rect(426, 339, 639, 479), (GXColor){ 255, 255, 255, 255 });
    }
};

int main(int argc, char **argv) {
    wit::Application app(wit::UseWiiRemote | wit::UseGameCube | wit::UseGX);

    MatGrid grid;
    app.installEventFilter(&grid);

    app.run();
    return 0;
}
