#include <wit/wit.h>
#include <wit/application.h>
#include <wit/font.h>
#include <wit/eventhandler.h>
#include <wit/event.h>
#include <wit/renderer.h>
#include <wit/console.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <string>

class HelloWorld : public wit::EventHandler, public wit::Renderer
{
public:
    HelloWorld() : wit::EventHandler(), wit::Renderer(), doShutdown(0), font("Sans16") {
        // initializers only
    }

    bool buttonPressEvent(wit::ButtonEvent* event) {
        doShutdown = 1;
        return true;
    }

    void render() {
        wit::Console* console = wit::Application::instance()->console();
        if(!font.isValid()) {
            console->setCursorPosition(5, 5);
            console->write("Font is invalid, exiting");
            exit(0);
        }

        if(doShutdown) {
            font.draw(150, 200, "Returning to loader...", (GXColor){ 255, 0, 0, 255 });
            if(++doShutdown > 2)
                witApp()->quit();
        } else {
            font.draw(150, 150, "Hello, world!", (GXColor){ 255, 255, 255, 255 });
        }
    }

    int x, y;
    int doShutdown;
    wit::Font font;
};

int main(int argc, char **argv) {
    wit::Application app(wit::UseWiiRemote | wit::UseGX | wit::UseConsole);
    HelloWorld hello;
    app.pushFocus(&hello);
    app.run();
    return 0;
}
