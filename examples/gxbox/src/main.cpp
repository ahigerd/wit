#include <wit/wit.h>
#include <wit/application.h>
#include <wit/console.h>
#include <wit/eventhandler.h>
#include <wit/event.h>
#include <wit/geometry.h>
#include <wit/renderer.h>

class HelloWorld : public wit::EventHandler, public wit::Renderer
{
public:
    HelloWorld() : wit::EventHandler(), wit::Renderer(), doShutdown(false) {
        x = 320;
        y = 240;
        dx = dy = 0;
    }

    bool buttonPressEvent(wit::ButtonEvent* event) {
        switch(event->button) {
        case wit::Button_Up:
            dy -= 1;
            break;
        case wit::Button_Down:
            dy += 1;
            break;
        case wit::Button_Left:
            dx -= 1;
            break;
        case wit::Button_Right:
            dx += 1;
            break;
        case wit::Button_Plus:
            setScale(scale() * 1.1);
            break;
        case wit::Button_Minus:
            setScale(scale() / 1.1);
            break;
        case wit::Button_Home:
            doShutdown = true;
            witApp()->quit();
            break;
        default:
            // ignore all other buttons for now
            ;
        };

        return true;
    }

    bool buttonReleaseEvent(wit::ButtonEvent* event) {
        switch(event->button) {
        case wit::Button_Up:
            dy += 1;
            break;
        case wit::Button_Down:
            dy -= 1;
            break;
        case wit::Button_Left:
            dx += 1;
            break;
        case wit::Button_Right:
            dx -= 1;
            break;
        default:
            // ignore all other buttons for now
            ;
        };
        return true;
    }

    void render() {
        x += dx;
        y += dy;
        drawRect(wit::Rect(x-50, y-50, x+50, y+50), (GXColor){ 0, 255, 0, 255 });
        fillRect(wit::Rect(x-25, y-25, x+25, y+25), (GXColor){ 0, 0, 255, 128 });
    }

    int x, y;
    int dx, dy;
    bool doShutdown;
};

int main(int argc, char **argv) {
    wit::Application app(wit::UseWiiRemote | wit::UseGameCube | wit::UseGX | wit::UseDoubleBuffer);
    app.setAbortGxOnReset(true);
    HelloWorld hello;
    app.pushFocus(&hello);
    app.run();
    return 0;
}
