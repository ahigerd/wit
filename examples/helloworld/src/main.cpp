#include <wit/wit.h>
#include <wit/application.h>
#include <wit/console.h>
#include <wit/eventhandler.h>
#include <wit/event.h>
#include <wit/renderer.h>

class HelloWorld : public wit::EventHandler, public wit::Renderer
{
public:
    HelloWorld() : wit::EventHandler(), wit::Renderer(), doShutdown(false) {
        wit::Console* console = witApp()->console();
        x = console->columns() / 2 - 5;
        y = console->rows() / 2;
    }

    bool buttonPressEvent(wit::ButtonEvent* event) {
        switch(event->button) {
        case wit::Button_Up:
            y--;
            break;
        case wit::Button_Down:
            y++;
            break;
        case wit::Button_Left:
            x--;
            break;
        case wit::Button_Right:
            x++;
            break;
        case wit::Button_Home:
            doShutdown = true;
            witApp()->quit();
            break;
        default:
            // ignore all other buttons for now
            ;
        };
        return true;
    }

    void render() {
        wit::Console* console = witApp()->console();
        console->clear();
        console->setCursorPosition(x, y);
        if(doShutdown)
            console->write("Returning to loader...");
        else
            console->write("Hello, world!");
    }

    int x, y;
    bool doShutdown;
};

int main(int argc, char **argv) {
    wit::Application app(wit::UseWiiRemote | wit::UseGameCube | wit::UseConsole);
    HelloWorld hello;
    app.pushFocus(&hello);
    app.run();
    return 0;
}
