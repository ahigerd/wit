#include <wit/wit.h>
#include <wit/application.h>

int main(int argc, char **argv) {
    // TODO: Only enable the features you need
    wit::Application app(wit::AllFeatures);

    // TODO: Put your startup code here

    app.run();
    return 0;
}
