#include <wit/wit.h>
#include <wit/application.h>
#include <wit/console.h>
#include <wit/eventhandler.h>
#include <wit/event.h>
#include <wit/renderer.h>
#include <wit/widget.h>
#include <stdio.h>

// Here we define a Renderer subclass that we will use for showing
// the current "status."
class Status : public wit::Renderer {
public:
    // Storage for status color
    GXColor color;

    // We don't need any special initialization for ourselves,
    // so just initialize the Renderer
    Status() : wit::Renderer() {}

    // The render() function is called on every Renderer every frame.
    void render() {
        // We draw a box in the middle of the screen filled with the
        // desired color and bordered in white
        fillRect(wit::Rect(250, 200, 400, 300), color);
        drawRect(wit::Rect(250, 200, 400, 300),
                 (GXColor){ 255, 255, 255, 255 });
    }
};
// We declare a global variable so that other objects can update it
Status* status = 0;

// Here we define a class that will handle the "mouse" pointer on the 
// screen. Since it will perform drawing, it inherits from Renderer,
// and since it has to track the Wii Remote position, it inherits
// from EventHandler.
class Pointer : public wit::EventHandler, public wit::Renderer
{
public:
    // Storage for the pointer shape and the current coordinates
    wit::Polygon cursor;
    float x, y;

    Pointer() : wit::EventHandler(), wit::Renderer() {
        // Here we define the shape of the pointer
        cursor.addPoint(0, 0);
        cursor.addPoint(8, 9);
        cursor.addPoint(4, 9);
        cursor.addPoint(6, 14);
        cursor.addPoint(5, 13);
        cursor.addPoint(3, 9);
        cursor.addPoint(0, 10);
    }

    // buttonPressEvent is fired whenever a button is pressed
    bool buttonPressEvent(wit::ButtonEvent* event) {
        // As a matter of convenience, the Pointer class also handles 
        // shutting down the program cleanly when the Home button is
        // pressed.
        if(event->button == wit::Button_Home) {
            witApp()->quit();
            // We return "true" here so that no other EventHandler
            // processes the button
            return true;
        }
        // We return "false" here so that any other button presses
        // are not consumed
        return false;
    }

    // pointerEvent is fired whenever a Wii Remote moves
    bool pointerEvent(wit::PointerEvent* event) {
        // We only want to follow the first Remote
        if(event->controllerNumber != 0) return false;
        // We need to keep track of the position so we can draw it 
        x = event->x;
        y = event->y;
        // As before, we want other objects to have a chance to
        // handle the event
        return false;
    }

    void render() {
        // Since the polygon is anchored at (0, 0) we use 
        // setTranslation to move where (0, 0) actually is.
        setTranslation(-x, -y);
        fillPolygon(cursor, (GXColor){ 0, 0, 0, 255 });
        drawPolygon(cursor, (GXColor){ 255, 255, 255, 255 });
    }

};

// This is a standard example of what wit::Widget is intended to do.
// Widgets are Hotspots, EventHandlers, and Renderers.
// See the wit::Widget documentation for more details.
class Button : public wit::Widget
{
public:
    GXColor color;

    Button() : wit::Widget() {}

    bool buttonPressEvent(wit::ButtonEvent* event) {
        // wit::Widget only accepts button events while the Wii
        // Remote is pointed at it.

        // These buttons make the status box change color when you
        // click on them with the A button.
        if(event->button == wit::Button_A)
            status->color = color;
        return true;
    }

    void render() {
        // We draw the bounding box of the hotspot on the screen
        wit::Rect bb = boundingBox();
        fillRect(bb, color);
        drawRect(bb, (GXColor){ 255, 255, 255, 255 });

        // If the Remote is currently pointing at the Button, we draw
        // a semi-transparent white box on it to make a highlight
        if(hasPointer())
            fillRect(bb, (GXColor){ 255, 255, 255, 128 });
    }
};

int main(int argc, char **argv) {
    // All WIT programs need an Application object. It will generally 
    // be the first thing you create in main(). This program uses the 
    // Wii Remote and GX, and tracks the remote's pointer.
    wit::Application app(wit::UseWiiRemote |
                         wit::UseGX |
                         wit::UsePointer);

    // We create the pointer and install it as an event filter in
    // order to receive every event.
    Pointer pointer;
    app.installEventFilter(&pointer);

    // Now we create four buttons in different parts of the screen, 
    // each a different color.
    Button b1;
    b1.color = (GXColor){ 255, 0, 0, 255 };
    b1.setRegion(wit::Rect(100, 100, 200, 150));

    Button b2;
    b2.color = (GXColor){ 0, 255, 0, 255 };
    b2.setRegion(wit::Rect(450, 100, 550, 150));

    Button b3;
    b3.color = (GXColor){ 0, 0, 255, 255 };
    b3.setRegion(wit::Rect(100, 350, 200, 400));

    Button b4;
    b4.color = (GXColor){ 128, 128, 128, 255 };
    b4.setRegion(wit::Rect(450, 350, 550, 400));

    // We also create the status box and make it black.
    Status m_status;
    m_status.color = (GXColor){ 0, 0, 0, 0 };

    // We make a pointer to the status box so the buttons can find it.
    status = &m_status;

    // Finally, we start the application's event loop.
    // run() will not return until something calls quit().
    app.run();
    return 0;
}
