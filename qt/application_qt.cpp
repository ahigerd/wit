#include "application.h"
#include "console.h"
#include "event.h"
#include "eventhandler.h"
#include "hotspot.h"
#include "application_qt.h"

#include <QSettings>
#include <QEventLoop>
#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QTimer>
#include <QPainter>
#include <QtDebug>

#include <string.h>
#include <algorithm>
#include <iterator>
#include <vector>

namespace wit {

Application* witInstance = 0; 
ApplicationPrivate* witPrivate = 0;

class DisplayScreen : public QWidget
{
public:
    DisplayScreen() : QWidget(0) {
        setAttribute(Qt::WA_NoSystemBackground, true);
        setMouseTracking(true);
    }

    void paintEvent(QPaintEvent*) {
        p.begin(this);
        p.drawImage(0, 0, frame);
        p.end();
    }

    QPainter p;
    QImage frame;
};


ApplicationPrivate::ApplicationPrivate(int argc, char** argv) 
: QApplication(argc, argv), currentBuffer(false), doShutdown(false), focus(false), eventFilters(false), window(0)
{
    // Default key mappings
    leftMouse = KeyMapping(WiiRemote, 0, Button_A);
    rightMouse = KeyMapping(WiiRemote, 0, Button_B);
    middleMouse = KeyMapping(WiiRemote, 0, Button_None);
    keyMappings[Qt::Key_Escape] = KeyMapping(WiiRemote, 0, Button_Home);
    keyMappings[Qt::Key_Left] = KeyMapping(WiiRemote, 0, Button_Left);
    keyMappings[Qt::Key_Right] = KeyMapping(WiiRemote, 0, Button_Right);
    keyMappings[Qt::Key_Up] = KeyMapping(WiiRemote, 0, Button_Up);
    keyMappings[Qt::Key_Down] = KeyMapping(WiiRemote, 0, Button_Down);
    keyMappings[Qt::Key_Z] = KeyMapping(WiiRemote, 0, Button_2);
    keyMappings[Qt::Key_X] = KeyMapping(WiiRemote, 0, Button_1);
    keyMappings[Qt::Key_D] = KeyMapping(WiiRemote, 0, Button_Plus);
    keyMappings[Qt::Key_F] = KeyMapping(WiiRemote, 0, Button_Minus);
}

void ApplicationPrivate::initFrameBuffer() {
    framebuffer[0] = QImage(screenWidth, 480, QImage::Format_ARGB32_Premultiplied);
    framebuffer[1] = framebuffer[0];
    display->setFixedSize(screenWidth, 480);
}

static KeyMapping stringToMapping(const QString& value)
{
    static KeyMapping noKey(WiiRemote, 0, Button_None);
    QStringList mapString = value.split(':');
    if(mapString.size() != 3) return noKey;
    KeyMapping mapping;
    mapping.controller = (Controller)mapString[0].toInt();
    mapping.controllerID = mapString[1].toInt();
    mapping.button = (Button)mapString[2].toInt();
    if(mapping.controller < WiiRemote || mapping.controller > GameCube) return noKey;
    if(mapping.controllerID < 0 || mapping.controllerID > 4) return noKey;
    if(mapping.button <= 0 || mapping.button >= Button_Max) return noKey; 
    return mapping;
}

static QString mappingToString(const KeyMapping& mapping)
{
    if(mapping.button == Button_None) return QString();
    return QString("%1:%2:%3").arg((int)mapping.controller).arg(mapping.controllerID).arg((int)mapping.button);
}

void ApplicationPrivate::loadKeyMappings()
{
    QSettings settings;
    settings.beginGroup("keyMappings");
    foreach(const QString& key, settings.childKeys()) {
        int keyID = key.mid(1).toInt();
        if(keyID < 0x20) continue; // invalid setting 
        keyMappings[keyID] = stringToMapping(settings.value(key).toString());
    }
    settings.endGroup();
    if(settings.contains("mouseMappings/left"))
        leftMouse = stringToMapping(settings.value("mouseMappings/left").toString());
    if(settings.contains("mouseMappings/right"))
        rightMouse = stringToMapping(settings.value("mouseMappings/right").toString());
    if(settings.contains("mouseMappings/middle"))
        middleMouse = stringToMapping(settings.value("mouseMappings/middle").toString());
    if(settings.contains("mouseMappings/x1"))
        x1Mouse = stringToMapping(settings.value("mouseMappings/x1").toString());
    if(settings.contains("mouseMappings/x2"))
        x2Mouse = stringToMapping(settings.value("mouseMappings/x2").toString());
}

void ApplicationPrivate::saveKeyMappings()
{
    QSettings settings;
    settings.setValue("mouseMappings/left", mappingToString(leftMouse));
    settings.setValue("mouseMappings/right", mappingToString(rightMouse));
    settings.setValue("mouseMappings/middle", mappingToString(middleMouse));
    settings.setValue("mouseMappings/x1", mappingToString(x1Mouse));
    settings.setValue("mouseMappings/x2", mappingToString(x2Mouse));
    settings.beginGroup("keyMappings");
    foreach(int key, keyMappings.keys()) {
        settings.setValue(QString("K%1").arg(key), mappingToString(keyMappings[key]));
    }
}

bool ApplicationPrivate::notify(QObject* obj, QEvent* event) {
    if(nativeHomeMenu && homeMenuVisible) {
        // If we're using a native home menu, and it's in use,
        // let Qt do all of the event handling because the game
        // loop is suspended.
        return QApplication::notify(obj, event);
    }

    int type = event->type();
    if(type == QEvent::Close) {
        witInstance->quit();
    } else if(type == QEvent::KeyPress || type == QEvent::KeyRelease) {
        if(static_cast<QKeyEvent*>(event)->isAutoRepeat()) return true;
        KeyMapping key = keyMappings[static_cast<QKeyEvent*>(event)->key()];
        if(key.button == Button_None) return false;
        if(autoHomeMenu && key.button == Button_Home && type == QEvent::KeyPress) {
            witInstance->showHomeMenu();
            return true;
        }
        witInstance->postEvent(new ButtonEvent((type == QEvent::KeyPress) ? Event::ButtonPress : Event::ButtonRelease, key.controller, key.controllerID, key.button));
        return true;
    } else if(type == QEvent::MouseButtonPress || type == QEvent::MouseButtonRelease) {
        KeyMapping key;
        if(static_cast<QMouseEvent*>(event)->button() == Qt::LeftButton)
            key = leftMouse;
        else if(static_cast<QMouseEvent*>(event)->button() == Qt::RightButton)
            key = rightMouse;
        if(static_cast<QMouseEvent*>(event)->button() == Qt::MiddleButton)
            key = middleMouse;
        witInstance->postEvent(new ButtonEvent((type == QEvent::MouseButtonPress) ? Event::ButtonPress : Event::ButtonRelease, key.controller, key.controllerID, key.button));
        return true;
    }
    // TODO: figure out how to use joysticks and/or wiiuse
    if(features & UsePointer) {
        if(type == QEvent::MouseMove && obj == display) {
            QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
            witInstance->postEvent(new PointerEvent(0, mouse->x(), mouse->y(), 3.0f));
            return true;
        } else if(type == QEvent::Leave && obj == display) {
            witInstance->postEvent(new PointerEvent(0, -1.0f, -1.0f, -1.0f));
            return true;
        }
    }
    // TODO: wiiuse
    return QApplication::notify(obj, event);
}

Application::Application(int features)
{
    // initialize private data
    int argc = 0;
    char* argv[] = { "wit_application", 0 };
    witInstance = this;
    witPrivate = d = new ApplicationPrivate(argc, argv);
    d->features = features;
    d->setOrganizationName("Alkahest");
    d->setOrganizationDomain("alkahest.com");
    d->setApplicationName("WIT Simulator");

    QSettings settings;
    d->loadKeyMappings();

    // initialize framebuffers and set up window
    d->window = new QMainWindow;
    d->window->setCentralWidget(d->display = new DisplayScreen);
    d->display->setMouseTracking(true);
    d->display->setCursor(Qt::BlankCursor);
    d->screenWidth = settings.value("widescreen", false).toBool() ? 854 : 640;
    d->painter = new QPainter;
    d->initFrameBuffer();

    // initialize console
    d->console = new Console(0);
}

Application::~Application()
{
    // clean up data structures
    EventHandlerNode* next;
    while(d->focus) {
        next = d->focus->next;
        delete d->focus;
        d->focus = next;
    }
    while(d->eventFilters) {
        next = d->eventFilters->next;
        delete d->eventFilters;
        d->eventFilters = next;
    }
    TimerNode* nextTimer;
    while(d->timers) {
        nextTimer = d->timers->next;
        delete d->timers;
        d->timers = nextTimer;
    }
    while(d->addTimers) {
        nextTimer = d->addTimers->next;
        delete d->addTimers;
        d->addTimers = nextTimer;
    }
    // shut down QApplication
    d->quit();
    delete d;
}

Application* Application::instance()
{
    return witInstance;
}

Console* Application::console() const
{
    return d->console;
}

int Application::features() const
{
    return d->features;
}

void* Application::framebuffer() const
{
    return d->framebuffer[d->currentBuffer ? 1 : 0].bits();
}

int Application::screenWidth() const
{
    return d->screenWidth;
}

int Application::screenHeight() const
{
    return 480;
}

void Application::setAutoHomeMenu(bool on)
{
    d->autoHomeMenu = on;
}

bool Application::autoHomeMenu() const
{
    return d->autoHomeMenu;
}

void Application::showHomeMenu()
{
    d->nativeHomeMenu = true;
    setHomeMenuVisible(true);
    if(!d->homeMenu) d->homeMenu = new HomeMenu(d->window);
    d->homeMenu->exec();
    hideHomeMenu();
}

void Application::hideHomeMenu()
{
    if(d->nativeHomeMenu && d->homeMenu) {
        d->saveKeyMappings();
        d->homeMenu->accept();
        delete d->homeMenu;
        d->homeMenu = 0;
    }
    d->nativeHomeMenu = false;
    setHomeMenuVisible(false);
}

void Application::homeMenuEvent(Event* event)
{
    // The Qt implementation uses a native Qt dialog and therefore
    // native Qt events. The home menu does not receive events.
    Q_UNUSED(event);
}

void Application::setHomeMenuVisible(bool on)
{
    d->homeMenuVisible = on;
}

bool Application::isHomeMenuVisible() const
{
    return d->homeMenuVisible;
}

void Application::reboot()
{
    shutdown();
}

void Application::shutdown(ShutdownMode)
{
    d->doShutdown = true;
    d->quit();
}

void Application::returnToMenu()
{
    shutdown();
}

void Application::quit()
{
    shutdown();
}

void Application::run()
{
    QEventLoop loop;
    d->doShutdown = false;

    d->window->show();

    QTimer frameTimer;
    frameTimer.setInterval(16);
    QObject::connect(&frameTimer, SIGNAL(timeout()), &loop, SLOT(quit()));
    frameTimer.start();

    int black = qRgba(0, 0, 0, 255);

    while(true) {
        // Process events until the frame timer ticks
        QImage& fb = d->framebuffer[d->currentBuffer ? 1 : 0];
        fb.fill(black);
        d->painter->begin(&fb);
        loop.exec();
        if(d->doShutdown) break;

        // Ask everything to update the framebuffer
        wit_render_all();
        if(d->doShutdown) break;

        // Copy the framebuffer to the screen
        d->painter->end();
        d->display->frame = fb;
        d->display->update();
        if(d->features & UseDoubleBuffer) 
            d->currentBuffer = !d->currentBuffer;

        // Only process timers if the home menu is not visible
        if(!d->homeMenuVisible) {
            // Add timers scheduled during event handling
            if(d->addTimers) {
                // Find the end of the linked list
                TimerNode* timer = d->addTimers;
                while(timer->next) timer = timer->next;
                timer->next = d->timers;
                d->timers = d->addTimers;
                d->addTimers = 0;
            }

            // Check for expiring timers
            TimerNode* timer = d->timers;
            TimerNode* previousTimer = 0;
            TimerNode* nextTimer;
            while(timer) {
                if(--timer->framesRemaining < 0) {
                    timer->handler->timerTriggered(timer->data);
                    if(previousTimer) {
                        previousTimer->next = timer->next;
                    } else {
                        d->timers = timer->next;
                    }
                    nextTimer = timer->next;
                    delete timer;
                    timer = nextTimer;
                } else {
                    previousTimer = timer;
                    timer = timer->next;
                }
            }

            // Append timers scheduled during timer dispatch
            if(previousTimer) {
                previousTimer->next = d->addTimers;
            } else {
                d->timers = d->addTimers;
            }
            d->addTimers = 0;
        }
    }
    d->painter->end();
}

static bool wit_Application_distribute_event(Event* event, EventHandlerNode* node)
{
    while(node) {
        bool cancel = node->handler->event(event);
        if(cancel) return true;
        node = node->next;
    }
    return false;
}

static HotspotList lastHotspots[4];

void Application::postEvent(Event* event)
{
    if(d->homeMenuVisible) {
        homeMenuEvent(event);
        delete event;
        return;
    }

    if(wit_Application_distribute_event(event, d->eventFilters)) {
        delete event;
        return;
    }

    if(event->type() == Event::Pointer) {
        PointerEvent* p = static_cast<PointerEvent*>(event);
        if(p->isValid()) {
            Hotspot* hs;
            std::auto_ptr<HotspotList> spots = Hotspot::findAtPoint(p->x, p->y);
            std::sort(spots->begin(), spots->end());
            HotspotList lostFocus;
            std::set_difference(lastHotspots[p->controllerNumber].begin(), lastHotspots[p->controllerNumber].end(), spots->begin(), spots->end(), std::back_inserter(lostFocus));
            lastHotspots[p->controllerNumber] = *spots;
            if(lostFocus.size() > 0) {
                Hotspot** end = &lostFocus.back();
                for(Hotspot** iter = &lostFocus.front(); iter <= end; iter++) {
                    hs = *iter;
                    PointerLeaveEvent leaveEvent(p->controllerNumber, hs);
                    if(!wit_Application_distribute_event(&leaveEvent, d->eventFilters) && hs->eventHandler())
                        hs->eventHandler()->event(&leaveEvent);
                }
            }
            if(spots->size() > 0) {
                Hotspot** end = &spots->back();
                Hotspot* receiver = 0;
                for(Hotspot** iter = &spots->front(); iter <= end; iter++) {
                    hs = *iter;
                    if(!hs->eventHandler()) continue;
                    if(receiver && hs->zIndex() < receiver->zIndex()) continue;
                    receiver = hs;
                }
                if(receiver)
                    receiver->eventHandler()->event(event);
            }
        }
    } else {
        wit_Application_distribute_event(event, d->focus);
    }
    delete event;
}

void Application::pushFocus(EventHandler* handler, int controllerID)
{
    EventHandlerNode* node = new EventHandlerNode;
    node->controllerID = controllerID;
    node->handler = handler;
    node->next = d->focus;
    d->focus = node;
}

EventHandler* Application::currentFocus(int controllerID) const
{
    if(!d->focus) return 0;
    EventHandlerNode* node = d->focus;
    while(node) {
        if(node->controllerID == controllerID || node->controllerID == -1)
            return node->handler;
        node = node->next;
    }
    return 0;
}

EventHandler* Application::popFocus(int controllerID)
{
    if(!d->focus) return 0;
    EventHandlerNode* node = d->focus;
    EventHandlerNode* lastNode = 0;
    while(node) {
        if(node->controllerID == controllerID) {
            if(lastNode) {
                lastNode->next = node->next;
            } else {
                d->focus = node->next;
            }
            EventHandler* h = node->handler;
            delete node;
            return h;
        }
        lastNode = node;
        node = node->next;
    }
    return 0;
}

void Application::removeFromFocusStack(EventHandler* handler, int controllerID)
{
    EventHandlerNode* node = d->focus;
    EventHandlerNode* lastNode = 0;
    while(node) {
        if(node->handler == handler && (controllerID == -1 || controllerID == node->controllerID)) {
            if(lastNode) {
                lastNode->next = node->next;
            } else {
                d->focus = node->next;
            }
            EventHandlerNode* delNode = node;
            node = node->next;
            delete delNode;
        } else {
            lastNode = node;
            node = node->next;
        }
    }
}

void Application::installEventFilter(EventHandler* handler)
{
    EventHandlerNode* node = new EventHandlerNode;
    node->controllerID = -1;
    node->handler = handler;
    node->next = d->eventFilters;
    d->eventFilters = node;
}

void Application::removeEventFilter(EventHandler* handler)
{
    EventHandlerNode* node = d->eventFilters;
    EventHandlerNode* lastNode = 0;
    while(node) {
        if(node->handler == handler) {
            if(lastNode) {
                lastNode->next = node->next;
            } else {
                d->eventFilters = node->next;
            }
            delete node;
            return;
        }
        lastNode = node;
        node = node->next;
    }
}

void Application::scheduleTimer(int frames, EventHandler* handler, void* data)
{
    TimerNode* timer = new TimerNode;
    timer->handler = handler;
    timer->framesRemaining = frames;
    timer->data = data;

    // We put the new timer in a secondary queue to avoid runaway callbacks if
    // a timer is scheduled during another timer's processing.
    timer->next = d->addTimers;
    d->addTimers = timer;
}

bool Application::abortGxOnReset() const
{
    return true;
}

void Application::setAbortGxOnReset(bool)
{
    /* no-op */
}

bool Application::isRemoteHorizontal(int) const
{
    /* no-op */
    return false;
}

void Application::setRemoteHorizontal(int, bool)
{
    /* no-op */
    // You don't turn your keyboard sideways.
}

void Application::resetButtonPressed()
{
    reboot();
}

void Application::powerButtonPressed(int controllerID)
{
    Q_UNUSED(controllerID);
    shutdown();
}

}

