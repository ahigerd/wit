#ifndef WIT_APPLICATION_QT_H
#define WIT_APPLICATION_QT_H

#include <QApplication>
#include <QMap>
#include <QMainWindow>
#include <QImage>
#include <QPainter>
#include "homemenu_qt.h"

namespace wit {

class Console;
class HomeMenu;
class DisplayScreen;

struct EventHandlerNode {
    EventHandler* handler;
    int controllerID;
    EventHandlerNode* next;
};

struct TimerNode {
    EventHandler* handler;
    int framesRemaining;
    void* data;
    TimerNode* next;
};

struct KeyMapping {
    KeyMapping() {}
    KeyMapping(Controller c, int i, Button b) : controller(c), controllerID(i), button(b) {}
  
    Controller controller;
    int controllerID;
    Button button;
};

class ApplicationPrivate : public QApplication {
friend class Application;
friend class HomeMenu;
public:
    ApplicationPrivate(int argc, char** argv);

    int features, screenWidth;
    bool currentBuffer, doShutdown;
    bool autoHomeMenu, homeMenuVisible, nativeHomeMenu;
    QImage framebuffer[2];
    EventHandlerNode* focus;
    EventHandlerNode* eventFilters;
    TimerNode* timers;
    TimerNode* addTimers;

    QMainWindow* window;
    DisplayScreen* display;
    QPainter* painter;

    QMap<int, KeyMapping> keyMappings;
    KeyMapping leftMouse, rightMouse, middleMouse, x1Mouse, x2Mouse;

    Console* console;
    HomeMenu* homeMenu;

    void initFrameBuffer();
    void loadKeyMappings();
    void saveKeyMappings();

    bool notify(QObject* obj, QEvent* event);
};

extern Application* witInstance; 
extern ApplicationPrivate* witPrivate;
void wit_render_all();

}

#endif
