#include "console.h"
#include <QtDebug>

namespace wit {

// The console is mostly dummied out here.
// Output appears on stderr.

Console::Console(void*) {}
Console::~Console() {}

void Console::clear() {}
void Console::setFramebuffer(void*) {}

int Console::rows() const { return 0; }
int Console::columns() const { return 0; }

void Console::setCursorPosition(int, int) {}
void Console::setCursorX(int) {}
void Console::setCursorY(int) {}
int Console::cursorX() const { return 0; }
int Console::cursorY() const { return 0; }

void Console::enableGecko(int, bool) {}
void Console::disableGecko() {}

void Console::write(const char* text)
{
    qDebug() << text;
}


}
