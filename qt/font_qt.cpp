#include "font.h"
#include "application.h"
#include "application_qt.h"

#include <QFont>
#include <QFontMetrics>
#include <QRegExp>
#include <QPointF>

#include <vector>
#include <map>
#include <string.h>

namespace wit {

FontSymbol::FontSymbol() : data(0), texWidth(0), texHeight(0), leftBearing(0), charWidth(0)
{
    // initializers only
}

struct FontPrivate {
    FontPrivate() : metrics(QFont()) {}
    bool registered;
    QFont font;
    QFontMetrics metrics;
    QMap<unsigned char, FontSymbol> symbolCache;
};

Font::Font() : d(new FontPrivate)
{
    d->registered = false;
}

Font::Font(const std::string& fontName, bool bold, bool italic) : d(new FontPrivate)
{ 
    // Parse the font name
    QString qFontName = QString::fromStdString(fontName);
    int fontSize = 16;
    if(qFontName.right(3).toInt() != 0) {
        fontSize = qFontName.right(3).toInt();
        qFontName.chop(3);
    } else if(qFontName.right(2).toInt() != 0) {
        fontSize = qFontName.right(2).toInt();
        qFontName.chop(2);
    } else if(qFontName.right(1).toInt() != 0) {
        fontSize = qFontName.right(1).toInt();
        qFontName.chop(1);
    }

    d->registered = true;
    d->font = QFont(qFontName, fontSize, bold ? QFont::Normal : QFont::Bold, italic);
    d->metrics = QFontMetrics(d->font);
}

Font::~Font()
{
    delete d;
}

bool Font::isValid() const
{
    return d->registered;
}

void Font::setChar(unsigned char, int, int, const char*, int, int)
{
    // For now, this is a no-op in Qt.
    // I may consider adding support for this to allow custom graphics later.
}

FontSymbol* Font::charData(unsigned char symbol) const
{
    if(!d->symbolCache.contains(symbol)) {
        FontSymbol rv;
        rv.data = 0;
        rv.texWidth = d->metrics.width(QString(1, symbol));
        rv.texHeight = d->metrics.height();
        rv.leftBearing = d->metrics.leftBearing(symbol);
        rv.charWidth = rv.texWidth;
        d->symbolCache[symbol] = rv;
    }
    return &d->symbolCache[symbol];
}

void Font::registerFont(const std::string&, bool, bool, int)
{
    // no-op in Qt
}

float Font::draw(float x, float y, const std::string& text, const GXColor& color)
{
    if(!d->registered) return x;

    QString qText = QString::fromStdString(text);

    witPrivate->painter->save();
    witPrivate->painter->setFont(d->font);
    witPrivate->painter->setPen(QPen(QColor(color.r, color.g, color.b, color.a)));
    witPrivate->painter->setBrush(QBrush(QColor(color.r, color.g, color.b, color.a)));
    witPrivate->painter->drawText(QPointF(x, y), qText);
    witPrivate->painter->restore();

    return x + d->metrics.width(qText);
}

}
