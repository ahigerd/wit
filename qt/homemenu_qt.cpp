#include "homemenu_qt.h"
#include <QBoxLayout>
#include <QScrollArea>
#include <QDialogButtonBox>
#include <QKeySequence>
#include <QLabel>
#include <QKeyEvent>
#include <QScrollBar>
#include <QTimer>
#include <QtAlgorithms>

namespace wit {

HomeMenu::HomeMenu(QWidget* parent) : QDialog(parent)
{
    setWindowTitle("Configuration");

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    scrollArea = new QScrollArea(this);
    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, this);
    QAbstractButton* addKey = buttons->addButton("Add", QDialogButtonBox::ActionRole);
    mainLayout->addWidget(scrollArea, 1);
    mainLayout->addWidget(buttons, 0);

    panel = new QWidget;
    scrollArea->setWidget(panel);
    scrollArea->setWidgetResizable(true);
    keyLayout = new QVBoxLayout(panel);
    keyLayout->setContentsMargins(1,1,1,1);
    keyLayout->setSpacing(1);

    KeyLine* left = new KeyLine();
    left->setMouseButton(Qt::LeftButton);
    left->setMapping(witPrivate->leftMouse);
    keyLayout->addWidget(left, 0);
    KeyLine* right = new KeyLine();
    right->setMouseButton(Qt::RightButton);
    right->setMapping(witPrivate->rightMouse);
    keyLayout->addWidget(right, 0);
    KeyLine* middle = new KeyLine();
    middle->setMouseButton(Qt::MiddleButton);
    middle->setMapping(witPrivate->middleMouse);
    keyLayout->addWidget(middle, 0);
    KeyLine* x1 = new KeyLine();
    x1->setMouseButton(Qt::XButton1);
    keyLayout->addWidget(x1, 0);
    x1->setMapping(witPrivate->x1Mouse);
    KeyLine* x2 = new KeyLine();
    x2->setMouseButton(Qt::XButton2);
    x2->setMapping(witPrivate->x2Mouse);
    keyLayout->addWidget(x2, 0);
    keyLines << left << right << middle << x1 << x2;

    QList<int> keys = witPrivate->keyMappings.keys();
    qSort(keys);
    foreach(int key, keys) {
        if(witPrivate->keyMappings[key].button == Button_None) continue;
        KeyLine* line = new KeyLine();
        line->setKey(key);
        line->setMapping(witPrivate->keyMappings[key]);
        keyLayout->addWidget(line, 0);
        keyLines << line;
        QObject::connect(line, SIGNAL(destroyed()), this, SLOT(senderDeleted()));
    }
    keyLayout->addStretch(1);

    panel->updateGeometry();
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setMinimumWidth(keyLayout->sizeHint().width() + 120);

    setCursor(Qt::ArrowCursor);
    QObject::connect(buttons, SIGNAL(accepted()), this, SLOT(saveSettings()));
    QObject::connect(buttons, SIGNAL(rejected()), this, SLOT(close()));
    QObject::connect(addKey, SIGNAL(clicked()), this, SLOT(newKey()));
}

void HomeMenu::scrollToBottom() 
{
    scrollArea->verticalScrollBar()->triggerAction(QScrollBar::SliderToMaximum);
}

void HomeMenu::newKey()
{
    KeyLine* line = new KeyLine();
    keyLayout->insertWidget(keyLayout->count() - 1, line, 0);
    keyLines << line;
    QObject::connect(line, SIGNAL(destroyed()), this, SLOT(senderDeleted()));
    QTimer::singleShot(1, this, SLOT(scrollToBottom()));
}

void HomeMenu::saveSettings()
{
    witPrivate->keyMappings.clear();
    foreach(KeyLine* line, keyLines) {
        KeyMapping mapping(
            (Controller)line->controllerType->currentIndex(),
            line->controllerNumber->currentIndex(),
            (Button)line->button->itemData(line->button->currentIndex()).toInt()
        );
        if(line->keyID > 0) {
            witPrivate->keyMappings[line->keyID] = mapping;
        } else if(line->mouseButton == Qt::LeftButton) {
            witPrivate->leftMouse = mapping;
        } else if(line->mouseButton == Qt::RightButton) {
            witPrivate->rightMouse = mapping;
        } else if(line->mouseButton == Qt::MiddleButton) {
            witPrivate->middleMouse = mapping;
        } else if(line->mouseButton == Qt::XButton1) {
            witPrivate->x1Mouse = mapping;
        } else if(line->mouseButton == Qt::XButton2) {
            witPrivate->x2Mouse = mapping;
        }
    }
    close();
}

void HomeMenu::senderDeleted()
{
    keyLines.removeAll(static_cast<KeyLine*>(sender()));
}

KeyLine::KeyLine() : QWidget(0), keyID(0), mouseButton(0)
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(1);
    keyButton = new QPushButton("Select Key", this);
    keyButton->setFixedWidth(125);
    controllerType = new QComboBox(this);
    controllerNumber = new QComboBox(this);
    button = new QComboBox(this);

    controllerType->addItem("Wii Remote");
    controllerType->addItem("Nunchuk");
    controllerType->addItem("Classic");
    controllerType->addItem("Guitar");
    controllerType->addItem("Balance Board");
    controllerType->addItem("GameCube");
    setController(WiiRemote);
    controllerNumber->addItem("#1");
    controllerNumber->addItem("#2");
    controllerNumber->addItem("#3");
    controllerNumber->addItem("#4");

    layout->addWidget(keyButton);
    layout->addWidget(controllerType);
    layout->addWidget(controllerNumber);
    layout->addWidget(button);

    QObject::connect(controllerType, SIGNAL(currentIndexChanged(int)), this, SLOT(setController(int)));
    QObject::connect(keyButton, SIGNAL(clicked()), this, SLOT(promptForKey()));
}

void KeyLine::promptForKey()
{
    KeyPrompt prompt(this);
    if(prompt.key == 0) return;
    if(prompt.key == -1) {
        deleteLater();
    } else {
        setKey(prompt.key);
    }
}

void KeyLine::setKey(int key)
{
    mouseButton = 0;
    keyID = key;
    keyButton->setText(QKeySequence(key).toString(QKeySequence::NativeText));
}

void KeyLine::setMouseButton(Qt::MouseButton button)
{
    mouseButton = (int)button;
    keyID = 0;
    if(button == Qt::LeftButton)
        keyButton->setText("Left Mouse");
    else if(button == Qt::RightButton)
        keyButton->setText("Right Mouse");
    else if(button == Qt::MiddleButton)
        keyButton->setText("Middle Mouse");
    else if(button == Qt::XButton1)
        keyButton->setText("Mouse X1");
    else if(button == Qt::XButton2)
        keyButton->setText("Mouse X2");
    keyButton->setEnabled(false);
}

void KeyLine::setMapping(const KeyMapping& mapping)
{
    setController((int)mapping.controller);
    controllerNumber->setCurrentIndex(mapping.controllerID);
    button->setCurrentIndex(button->findData((int)mapping.button));
}

void KeyLine::setController(int controller)
{
    controllerType->setCurrentIndex(controller);
    int currentIndex = button->currentIndex();
    int buttonID = currentIndex >= 0 ? button->itemData(currentIndex).toInt() : Button_None;
    button->clear();
    if(controller == WiiRemote || controller == Guitar || controller == Classic || controller == GameCube) {
        button->addItem("Up", Button_Up);
        button->addItem("Down", Button_Down);
        if(controller != Guitar) {
            button->addItem("Left", Button_Left);
            button->addItem("Right", Button_Right);
        }
    }
    if(controller == WiiRemote) {
        button->addItem("1", Button_1);
        button->addItem("2", Button_2);
    }
    if(controller == WiiRemote || controller == Classic || controller == GameCube) {
        button->addItem("A", Button_A);
        button->addItem("B", Button_B);
        if(controller != WiiRemote) {
            button->addItem("X", Button_X);
            button->addItem("Y", Button_Y);
        }
        if(controller == GameCube) {
            button->addItem("Z", Button_Z);
        } else if(controller == Classic) {
            button->addItem("ZL", Button_ZL);
            button->addItem("ZR", Button_ZR);
        }
        if(controller != WiiRemote) {
            button->addItem("L", Button_LClick);
            button->addItem("R", Button_RClick);
        }
    }
    if(controller == Nunchuk) {
        button->addItem("C", Button_C);
        button->addItem("Z", Button_Z);
    } else if(controller == GameCube) {
        button->addItem("Start", Button_Start);
    } else if(controller == Guitar) {
        button->addItem("Yellow", Button_Yellow);
        button->addItem("Green", Button_Green);
        button->addItem("Blue", Button_Blue);
        button->addItem("Red", Button_Red);
        button->addItem("Orange", Button_Orange);
    }
    if(controller == WiiRemote || controller == Guitar || controller == Classic) {
        button->addItem("-", Button_Minus);
        button->addItem("+", Button_Plus);
        if(controller != Guitar) {
            button->addItem("Home", Button_Home);
        }
    }
    button->setCurrentIndex(button->findData(buttonID));
}

KeyPrompt::KeyPrompt(QWidget* parent) : QDialog(parent), key(0)
{
    QVBoxLayout* layout1 = new QVBoxLayout(this);
    layout1->addWidget(new QLabel("Press the key you want to use.", this), 1);
    QHBoxLayout* layout2 = new QHBoxLayout;
    QPushButton* cancelButton = new QPushButton("Cancel", this);
    QPushButton* removeButton = new QPushButton("Remove Mapping", this);
    layout2->addStretch(1);
    layout2->addWidget(cancelButton, 0);
    layout2->addWidget(removeButton, 0);
    layout2->addStretch(1);
    layout1->addLayout(layout2, 0);

    QObject::connect(removeButton, SIGNAL(clicked()), this, SLOT(remove()));
    QObject::connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    show();
    grabKeyboard();
    exec();
}

void KeyPrompt::keyPressEvent(QKeyEvent* event)
{
    releaseKeyboard();
    key = event->key();
    accept();
}

void KeyPrompt::remove()
{
    key = -1;
    reject();
}

}
