#ifndef WIT_HOMEMENU_QT_H
#define WIT_HOMEMENU_QT_H

#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include "application.h"
#include "application_qt.h"
class QVBoxLayout;
class QScrollArea;

namespace wit {

class KeyMapping;
class KeyLine;

class HomeMenu : public QDialog {
Q_OBJECT
public:
    HomeMenu(QWidget* parent = 0);

public slots:
    void saveSettings();
    void newKey();
    void scrollToBottom();

private slots:
    void senderDeleted();

private:
    QList<KeyLine*> keyLines;
    QWidget* panel;
    QVBoxLayout* keyLayout;
    QScrollArea* scrollArea;
};

class KeyLine : public QWidget {
Q_OBJECT
public:
    KeyLine();

    int keyID;
    int mouseButton;
    QPushButton* keyButton;
    QComboBox* controllerType;
    QComboBox* controllerNumber;
    QComboBox* button;

public slots:
    void promptForKey();
    void setKey(int key);
    void setMouseButton(Qt::MouseButton button);
    void setMapping(const KeyMapping& mapping);
    void setController(int controller);
};

class KeyPrompt : public QDialog {
Q_OBJECT
public:
    KeyPrompt(QWidget* parent);

    int key;

private slots:
    void remove();

protected:
    void keyPressEvent(QKeyEvent* event);
};

}

#endif
