#include "renderer.h"
#include "application.h"
#include "application_qt.h"

#include <QTransform>

namespace wit {

// doubly-linked list for faster deletes
struct RendererPrivate {
    RendererPrivate* prev;
    Renderer* renderer;
    float scale, transx, transy; 
    RendererPrivate* next;
};

static RendererPrivate* wit_render_list = 0;

void wit_render_all()
{
    RendererPrivate* node = wit_render_list;
    while(node) {
        node->renderer->render();
        node = node->next;
    }
    node = wit_render_list;
    while(node) {
        node->renderer->waitForRendering();
        node = node->next;
    }
}

Renderer::Renderer()
{
    d = new RendererPrivate;
    d->prev = 0;
    d->renderer = this;
    d->scale = 1.0;
    d->transx = d->transy = 0.0;
    d->next = wit_render_list;
    if(wit_render_list)
        wit_render_list->prev = d;
    wit_render_list = d;
}

Renderer::~Renderer()
{
    if(d->prev) 
        d->prev->next = d->next;
    else
        wit_render_list = d->next;
    if(d->next)
        d->next->prev = d->prev;
    delete d;
}

void Renderer::waitForRendering()
{
    // do nothing
}

static void wit_renderer_transform(RendererPrivate* d)
{
    witPrivate->painter->setTransform(QTransform(d->scale, 0, 0, d->scale, -d->transx, -d->transy));
}

static void wit_renderer_setup(RendererPrivate* d)
{
    witPrivate->painter->save();
    wit_renderer_transform(d);
}

float Renderer::scale() const
{
    return d->scale;
}

void Renderer::setScale(float mag)
{
    d->scale = mag;
}

float Renderer::translateX() const 
{
    return d->transx;
}

float Renderer::translateY() const
{
    return d->transy;
}

void Renderer::setTranslation(float x, float y)
{
    d->transx = x;
    d->transy = y;
}

void Renderer::drawPoint(float x, float y, const GXColor& color)
{
    wit_renderer_setup(d);

    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));
    witPrivate->painter->drawPoint(QPointF(x, y));

    witPrivate->painter->restore();
}

void Renderer::drawPoint(const Point& p, const GXColor& color)
{
    drawPoint(p.x, p.y, color);
}

void Renderer::drawLine(float x1, float y1, float x2, float y2, const GXColor& color)
{
    wit_renderer_setup(d);

    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));
    witPrivate->painter->drawLine(QPointF(x1, y1), QPointF(x2, y2));

    witPrivate->painter->restore();
}

void Renderer::drawLine(const Point& p1, const Point& p2, const GXColor& color)
{
    drawLine(p1.x, p1.y, p2.x, p2.y, color);
}

void Renderer::drawRect(const Rect& rect, const GXColor& color)
{
    wit_renderer_setup(d);

    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));
    witPrivate->painter->setBrush(QBrush());
    witPrivate->painter->drawRect(QRectF(rect.x1, rect.y1, rect.x2-rect.x1, rect.y2-rect.y1));

    witPrivate->painter->restore();
}

void Renderer::fillRect(const Rect& rect, const GXColor& color)
{
    wit_renderer_setup(d);

    witPrivate->painter->fillRect(QRectF(rect.x1, rect.y1, rect.x2-rect.x1, rect.y2-rect.y1), QColor(color.r, color.g, color.b, color.a));

    witPrivate->painter->restore();
}

void Renderer::drawPoints(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();
    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));

    for(int i = 0; i < ct; i++) {
        witPrivate->painter->drawPoint(QPointF(points[i].x, points[i].y));
    }

    witPrivate->painter->restore();
}

void Renderer::drawPolygon(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();
    QPolygonF polygon;

    for(int i = 0; i < ct; i++) {
        polygon << QPointF(points[i].x, points[i].y);
    }

    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));
    witPrivate->painter->setBrush(QBrush());

    witPrivate->painter->drawPolygon(polygon);

    witPrivate->painter->restore();
}

void Renderer::fillPolygon(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();
    QPolygonF polygon;

    for(int i = 0; i < ct; i++) {
        polygon << QPointF(points[i].x, points[i].y);
    }

    witPrivate->painter->setPen(QColor(color.r, color.g, color.b, color.a));
    witPrivate->painter->setBrush(QColor(color.r, color.g, color.b, color.a));

    witPrivate->painter->drawPolygon(polygon);

    witPrivate->painter->restore();
}

}
