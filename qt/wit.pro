TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ../src .. .
DEFINES += WIT_USE_QT=1 WIT_QT_BUILD=1

HEADERS += ../src/application.h ../src/console.h ../src/event.h ../src/eventhandler.h ../src/font.h ../src/geometry.h ../src/hotspot.h ../src/renderer.h ../src/widget.h ../src/wit.h
HEADERS += application_qt.h wit_qt.h homemenu_qt.h
SOURCES += application_qt.cpp ../src/event.cpp ../src/eventhandler.cpp font_qt.cpp console_qt.cpp ../src/geometry.cpp ../src/hotspot.cpp renderer_qt.cpp ../src/widget.cpp homemenu_qt.cpp

headers.path = ../wit
for(header,HEADERS){
    headers.files += $$header
}
target.path = ../wit

INSTALLS += headers target
