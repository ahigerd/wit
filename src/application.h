#ifndef WIT_APPLICATION_H
#define WIT_APPLICATION_H

#include "wit.h"

namespace wit {

class ApplicationPrivate;
class Console;
class Event;
class EventHandler;

class Application {
friend class ApplicationPrivate;
public:
    Application(int features = AllFeatures);
    virtual ~Application();

    static Application* instance();
    Console* console() const;
    int features() const;

    void* framebuffer() const;
    int screenWidth() const;
    int screenHeight() const;

    void setAutoHomeMenu(bool on);
    bool autoHomeMenu() const;
    bool isHomeMenuVisible() const;
    virtual void showHomeMenu();
    virtual void hideHomeMenu();

    void reboot();
    void shutdown(ShutdownMode mode = Shutdown_Auto);
    void returnToMenu();
    void quit();

    void run();

    virtual void postEvent(Event* event);

    void pushFocus(EventHandler* handler, int controllerID = -1);
    EventHandler* currentFocus(int controllerID = -1) const;
    EventHandler* popFocus(int controllerID = -1);
    void removeFromFocusStack(EventHandler* handler, int controllerID = -1);

    void installEventFilter(EventHandler* handler);
    void removeEventFilter(EventHandler* handler);
    void scheduleTimer(int frames, EventHandler* handler, void* data);

    bool abortGxOnReset() const;
    void setAbortGxOnReset(bool on);

    bool isRemoteHorizontal(int controllerID) const;
    void setRemoteHorizontal(int controllerID, bool on);

protected:
    virtual void resetButtonPressed();
    virtual void powerButtonPressed(int controllerID);
    virtual void homeMenuEvent(Event* event);
    void setHomeMenuVisible(bool on);

private:
    ApplicationPrivate* d;
};

/**
 * Returns a pointer to the Application object.
 *
 * This is a convenience function equivalent to wit::Application::instance().
 *
 * \global
 * \sa Application
 */
 
inline Application* witApp() { return Application::instance(); }

}

using wit::witApp;

#endif
