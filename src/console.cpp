#include "console.h"
#include "application.h"
#include <ogcsys.h>
#include <stdio.h>
#include <string.h>

namespace wit {

/**
 * \class Console console.h wit/console.h
 *
 * The Console class provides access to the text console and, if a USBGecko
 * is installed, the debugging console.
 *
 * Console is not intended to be subclassed or instantiated by user code.
 * The only way to access the console is by using wit::Application::console().
 *
 * All console functions are safe to invoke even if the console is disabled;
 * they will simply do nothing.
 *
 * Note: The console is not compatible with wit::UseGX or wit::UseDoubleBuffer.
 * Specifying wit::UseConsole with one of these flags will cause the console
 * to be disabled.
 */

class ConsolePrivate {
friend class Console;
public:
    GXRModeObj* videoMode;
};

Console::Console(void* videoMode)
{
    d = new ConsolePrivate; // for future expansion
    memset(d, 0, sizeof(ConsolePrivate));
    d->videoMode = reinterpret_cast<GXRModeObj*>(videoMode);
    setFramebuffer(Application::instance()->framebuffer());
}

/**
 * Destroys the console handler.
 *
 * User code should not delete the Console object.
 */
Console::~Console()
{
    delete d;
}

void Console::setFramebuffer(void* fb)
{
	CON_Init(fb, 20, 20, d->videoMode->fbWidth, d->videoMode->xfbHeight, d->videoMode->fbWidth * VI_DISPLAY_PIX_SZ);
}

/**
 * Returns the number of text rows available on the console.
 */
int Console::rows() const
{
    if(!this)
        return 0;
    int r, c;
    CON_GetMetrics(&c, &r);
    return r;
}

/**
 * Returns the number of text columns available on the console.
 */
int Console::columns() const
{
    if(!this)
        return 0;
    int r, c;
    CON_GetMetrics(&c, &r);
    return c;
}

/**
 * Positions the text cursor on the screen. The next call to
 * wit::Console::write() will begin at this position.
 */
void Console::setCursorPosition(int x, int y)
{
    if(!this)
        return;
	printf("\x1b[%d;%dH", y, x);
}

/**
 * Positions the text cursor at the specified column, leaving
 * the row unchanged. The next call to wit::Console::write()
 * will begin at this position.
 */
void Console::setCursorX(int x)
{
    setCursorPosition(x, cursorY());
}

/**
 * Positions the text cursor at the specified row, leaving
 * the column unchanged. The next call to wit::Console::write()
 * will begin at this position.
 */
void Console::setCursorY(int y)
{
    setCursorPosition(cursorX(), y);
}

/**
 * Returns the current horizontal position of the cursor.
 */
int Console::cursorX() const
{
    if(!this)
        return 0;
    int x, y;
    CON_GetPosition(&x, &y);
    return x;
}

/**
 * Returns the current vertical position of the cursor.
 */
int Console::cursorY() const
{
    if(!this)
        return 0;
    int x, y;
    CON_GetPosition(&x, &y);
    return x;
}

/**
 * Enables the debugging console on a USBGecko.
 *
 * TODO: Figure out what the parameters are for
 */
void Console::enableGecko(int channel, bool safe)
{
    if(!this)
        return;
    CON_EnableGecko(channel, safe ? 1 : 0);
}

/**
 * Disables the USBGecko debugging console.
 */
void Console::disableGecko()
{
    if(!this)
        return;
    CON_EnableGecko(-1, false);
}

/**
 * Writes a string to the text console at the current cursor position.
 */
void Console::write(const char* text)
{
    if(Application::instance()->console() == 0)
        return;
    printf(text);
}

/**
 * Clears the screen.
 *
 * Note that this clears the framebuffer, not just the console.
 */
void Console::clear()
{
    Console::write("\x1b[2J");
}

}
