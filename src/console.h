#ifndef WIT_CONSOLE_H
#define WIT_CONSOLE_H

namespace wit {

class ConsolePrivate;
class Console {
friend class ConsolePrivate;
friend class Application;
public:
    ~Console();

    int rows() const;
    int columns() const;

    void setCursorPosition(int x, int y);
    void setCursorX(int x);
    void setCursorY(int y);
    int cursorX() const;
    int cursorY() const;

    void enableGecko(int channel, bool safe);
    void disableGecko();

    static void write(const char* text);
    static void clear();

private:
    Console(void* videoMode);
    void setFramebuffer(void*);
    ConsolePrivate* d;
};

}

#endif
