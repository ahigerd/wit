#include "eventhandler.h"
#include "event.h"
#include "application.h"

namespace wit {

/**
 * \class EventHandler eventhandler.h wit/eventhandler.h 
 *
 * The EventHandler class is the base class for all objects that wish to process events.
 *
 * On its own, EventHandler does very little. An EventHandler object must be installed as
 * an event filter (see Application::installEventFilter()), pushed on a focus stack (see
 * Application::pushFocus()), or assigned to a region of the screen (see Hotspot) in order
 * to receive events. When installed as an event filter, the object will receive every
 * event processed by WIT. When pushed onto a focus stack, the object will receive
 * non-pointer events from the associated controller. When assigned to a region of the
 * screen, the object will receive pointer events when the Wii Remote is pointing at
 * the region.
 *
 * By default, none of EventHandler's functions do anything. You must subclass
 * EventHandler to implement behavior.
 */

/**
 * Destroys the EventHandler object.
 *
 * If the object was installed as an event filter or present on any focus stack, it is removed.
 */
EventHandler::~EventHandler()
{
    Application::instance()->removeEventFilter(this);
    Application::instance()->removeFromFocusStack(this);
}

/**
 * Processes an event.
 *
 * Return \em true from event() or one of the other event-handling functions
 * to consume the event, that is, to prevent the event from propagating to
 * the next object in the event chain or focus stack.
 *
 * For non-user event types, the default behavior invokes the appropriate
 * specialized event-handling function (which themselves by default do nothing
 * but return false); for user event types, the default behavior returns false.
 */
bool EventHandler::event(Event* event)
{
    switch(event->type()) {
    case Event::Joystick:
        return joystickEvent(static_cast<JoystickEvent*>(event));
    case Event::Analog:
        return analogEvent(static_cast<AnalogEvent*>(event));
    case Event::Motion:
        return motionEvent(static_cast<MotionEvent*>(event));
    case Event::Pointer:
        return pointerEvent(static_cast<PointerEvent*>(event));
    case Event::PointerLeave:
        return pointerLeaveEvent(static_cast<PointerLeaveEvent*>(event));
    case Event::ButtonPress:
        return buttonPressEvent(static_cast<ButtonEvent*>(event));
    case Event::ButtonRelease:
        return buttonReleaseEvent(static_cast<ButtonEvent*>(event));
    default:
        // user-type events are ignored by default
        return false;
    }
}

/**
 * Processes a joystick event.
 *
 * The default behavior returns false.
 * 
 * \sa event()
 */
bool EventHandler::joystickEvent(JoystickEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes an analog button event.
 *
 * The default behavior returns false.
 * 
 * \sa event()
 */
bool EventHandler::analogEvent(AnalogEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes a motion sensor event.
 *
 * The default behavior returns false.
 * 
 * \sa event()
 */
bool EventHandler::motionEvent(MotionEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes a pointer event.
 *
 * The default behavior returns false.
 *
 * \sa event()
 */
bool EventHandler::pointerEvent(PointerEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes a pointer leave event.
 *
 * The default behavior returns false.
 *
 * \sa event()
 */
bool EventHandler::pointerLeaveEvent(PointerLeaveEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes a non-analog button press event.
 *
 * The default behavior returns false.
 *
 * \sa event()
 */
bool EventHandler::buttonPressEvent(ButtonEvent* event)
{
    (void)event;
    return false;
}

/**
 * Processes a non-analog button release event.
 *
 * The default behavior returns false.
 *
 * \sa event()
 */
bool EventHandler::buttonReleaseEvent(ButtonEvent* event)
{
    (void)event;
    return false;
}

/**
 * Invoked when a timer requested using Application::scheduleTimer() expires.
 * \em data is the user data pointer passed to scheduleTimer().
 *
 * The default behavior does nothing.
 *
 * \sa Application::scheduleTimer()
 */
void EventHandler::timerTriggered(void* data)
{
    (void)data;
}

}
