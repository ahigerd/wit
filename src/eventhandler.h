#ifndef WIT_EVENTHANDLER_H
#define WIT_EVENTHANDLER_H

namespace wit {

class Event;
class JoystickEvent;
class AnalogEvent;
class MotionEvent;
class PointerEvent;
class PointerLeaveEvent;
class ButtonEvent;

class EventHandler {
public:
    virtual ~EventHandler();

    virtual bool event(Event* event);

    virtual bool joystickEvent(JoystickEvent* event);
    virtual bool analogEvent(AnalogEvent* event);
    virtual bool motionEvent(MotionEvent* event);
    virtual bool pointerEvent(PointerEvent* event);
    virtual bool pointerLeaveEvent(PointerLeaveEvent* event);
    virtual bool buttonPressEvent(ButtonEvent* event);
    virtual bool buttonReleaseEvent(ButtonEvent* event);
    virtual void timerTriggered(void* data);
};

}

#endif
