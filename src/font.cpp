#include "font.h"
#include "application.h"
#include <gctypes.h>
#include <ogc/gx_struct.h>
#include <ogc/cache.h>
#include <vector>
#include <map>
#include <string.h>
#include <malloc.h>

namespace wit {

/**
 * \class FontSymbol font.h wit/font.h
 *
 * The FontSymbol class encapsulates the data for a single glyph.
 *
 * For performance reasons, FontSymbol exposes its members directly instead of using
 * accessor and mutator methods.
 *
 * Generally you will only need to worry about FontSymbol objects if you are creating
 * or customizing fonts, or if you need to query the metrics of a given character.
 *
 * Be sure to read the warnings on the \ref data member.
 */

/**
 * \var char* FontSymbol::data
 *
 * A pointer to a buffer containing a texture in IA4 format. This buffer must remain
 * valid at all times. Most applications will store this buffer directly in the
 * application binary.
 *
 * As a result, all copies made from a FontSymbol object refer to the same buffer.
 */

/**
 * \var signed char FontSymbol::charWidth
 *
 * The advance width of the glyph, in pixels. This is the distance from the start of
 * the glyph's cell to the position that the next glyph should be drawn.
 */

/**
 * \var signed char FontSymbol::leftBearing
 *
 * The offset between the left side of the glyph's cell and the left side of the glyph
 * texture. A left bearing of 1, for example, will draw the glyph one pixel to the right
 * of the current position.
 */

/**
 * \var signed char FontSymbol::texWidth
 *
 * The width of the glyph's texture, in pixels. Since FontSymbol textures are in the IA4
 * format, the width is a multiple of 8.
 */

/**
 * \var signed char FontSymbol::texHeight
 *
 * The height of the glyph's texture, in pixels. Since FontSymbol textures are in the IA4
 * format, the height is a multiple of 4.
 */

/**
 * Creates a new FontSymbol object with all metrics set to 0 pixels and null data.
 *
 * You will not generally need to construct a FontSymbol object directly, as 
 * \ref Font::setChar() will create them for you.
 */
FontSymbol::FontSymbol() : data(0), texWidth(0), texHeight(0), leftBearing(0), charWidth(0)
{
    // initializers only
}

struct FontPrivate {
    bool registered;
    FontSymbol symbols[95];
    unsigned char wordSpacing;
};

struct FontRegistry {
    FontRegistry(const std::string& name) : name(name), next(0) {}
    std::string name;
    FontPrivate* normal;
    FontPrivate* bold;
    FontPrivate* italic;
    FontPrivate* boldItalic;
    FontRegistry* next;
};

static FontRegistry* wit_font_registry = 0;

static FontRegistry* wit_find_font(const std::string& name)
{
    FontRegistry* r = wit_font_registry;
    while(r) {
        if(r->name == name) return r;
        r = r->next;
    }
    return 0;
}

/**
 * \class Font font.h wit/font.h
 *
 * The Font class encapsulates a set of glyphs. 
 *
 * Fonts are stored in a specialized format, with each character stored as an IA4 texture.
 * This format allows text to be rendered very quickly. See the tools/font2wit application
 * bundled with WIT to generate a WIT font resource from a TrueType font.
 *
 * WIT comes bundled with one font, Sans16, which is only available in normal (non-bold,
 * non-italic) style. This font is derived from Bitstream Vera Sans, copyright &copy; 
 * 2003 by Bitstream, Inc. See src/Sans16.cpp for the full license of this font.
 *
 * Note: The Font class currently only supports 7-bit ASCII text. Newlines, tab characters,
 * other formatting characters, and word wrapping are currently unsupported. Characters
 * outside the range of 32 (space) to 126 ("~") are replaced with spaces. These are all
 * purely temporary limitations; support for UTF-8 is scheduled for a later release.
 * However, support for characters outside of the ASCII range will require explicit
 * support from the font. 
 */

/**
 * Creates a new, unregistered Font object with no glyphs.
 */
Font::Font() : d(new FontPrivate)
{
    d->registered = false;
    memset(d->symbols, 0, sizeof(d->symbols));
}

/**
 * Creates a new Font object loaded from an existing registered font.
 *
 * If the requested font has not been registered, the object will be invalid.
 *
 * \sa isValid
 */
Font::Font(const std::string& fontName, bool bold, bool italic) : d(0)
{
    FontRegistry* r = wit_find_font(fontName);
    if(r) {
        if(!bold && !italic)
            d = r->normal;
        else if(bold && !italic)
            d = r->bold;
        else if(!bold && italic)
            d = r->italic;
        else
            d = r->boldItalic;
    }
}

/**
 * Destroys the Font object.
 */
Font::~Font()
{
    if(d && !d->registered)
        delete d;
}

/**
 * Returns true if the font is valid. A valid font is a font created with the
 * default constructor or one that was successfully loaded from a registered
 * font. 
 */
bool Font::isValid() const
{
    return d;
}

/**
 * Sets the glyph for the specified character to the (texWidth) x (texHeight)
 * texture pointed to by data. The character is given the specified left bearing
 * and advance width.
 *
 * The buffer pointed to by data must remain valid for the lifetime of the application.
 */
void Font::setChar(unsigned char symbol, int texWidth, int texHeight, const char* data, int leftBearing, int width)
{
    if(!d || symbol < 33 || symbol > 126) return;
    int length = texWidth * texHeight;
    symbol -= 33;
    FontSymbol s;
    s.texWidth = texWidth;
    s.texHeight = texHeight;
    s.data = (char*)memalign(32, length);
    if(s.data) memcpy(s.data, data, length);
    s.leftBearing = leftBearing;
    s.charWidth = width;
    d->symbols[symbol] = s;
    DCFlushRange(s.data, length);
}

/**
 * Returns a FontSymbol object containing information about the requested
 * glyph from the font.
 */
FontSymbol* Font::charData(unsigned char symbol) const
{
    if(!d || symbol < 33 || symbol > 126) return 0;
    return &d->symbols[symbol - 33];
}

/**
 * Registers the current font with the specified name, style, and word spacing. Once a
 * font has been registered it can be loaded using the constructor.
 */
void Font::registerFont(const std::string& fontName, bool bold, bool italic, int wordSpacing)
{
    if(!d || d->registered) return;

    d->registered = true;

    FontRegistry* r = wit_find_font(fontName);
    if(!r) {
        r = new FontRegistry(fontName);
        r->normal = 0;
        r->bold = 0;
        r->italic = 0;
        r->boldItalic = 0;
        r->next = wit_font_registry;
        wit_font_registry = r;
    }

    d->wordSpacing = wordSpacing;

    if(!bold && !italic)
        r->normal = d;
    else if(bold && !italic)
        r->bold = d;
    else if(!bold && italic)
        r->italic = d;
    else
        r->boldItalic = d;
}

/**
 * Renders a string of text in the specified color at the specified location on the screen. 
 */
float Font::draw(float x, float y, const std::string& text, const GXColor& color)
{
    if(!d || !d->registered) return x;

    // TODO: add font rendering to Renderer to allow transformations and easier text widgets

    Mtx44 perspective;
    guOrtho(perspective, 0, wit::Application::instance()->screenHeight(), 0, wit::Application::instance()->screenWidth(), 0, 300);
    GX_LoadProjectionMtx(perspective, GX_ORTHOGRAPHIC);
    
    GX_SetNumChans(1);
    GX_SetNumTexGens(1);
    GX_SetTevOp(GX_TEVSTAGE0, GX_MODULATE);
    GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
    GX_SetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);

    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_TEX0, GX_DIRECT);

    int ct = text.size();
    GXTexObj texObj;
    for(int i = 0; i < ct; i++) {
        if(text[i] < 33) {
            x += d->wordSpacing;
            continue;
        }
        FontSymbol* s = charData(text[i]);
        if(!s || !s->data) continue;
        x += int(s->leftBearing);

        GX_InitTexObj(&texObj, s->data, s->texWidth, s->texHeight, GX_TF_IA4, GX_CLAMP, GX_CLAMP, GX_FALSE);
        GX_LoadTexObj(&texObj, GX_TEXMAP0);
        GX_InvalidateTexAll();

        GX_Begin(GX_QUADS, GX_VTXFMT7, 4); // TODO: pass all quads at once
        GX_Position2f32(x, y);
        GX_Color4u8(color.r, color.g, color.b, color.a);
        GX_TexCoord2f32(0, 0);
        GX_Position2f32(x + s->texWidth, y);
        GX_Color4u8(color.r, color.g, color.b, color.a);
        GX_TexCoord2f32(1, 0);
        GX_Position2f32(x + s->texWidth, y + s->texHeight);
        GX_Color4u8(color.r, color.g, color.b, color.a);
        GX_TexCoord2f32(1, 1);
        GX_Position2f32(x, y + s->texHeight);
        GX_Color4u8(color.r, color.g, color.b, color.a);
        GX_TexCoord2f32(0, 1);
        GX_End();
        x += int(s->charWidth) - int(s->leftBearing);
    }

    return x;
}

}
