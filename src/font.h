#ifndef WIT_FONT_H
#define WIT_FONT_H

#include <string>
#ifdef WIT_USE_QT
#include <wit.h>
#else
#include <ogc/gx.h>
#endif

namespace wit {

struct FontSymbol {
    FontSymbol();
    char* data;
    signed char texWidth, texHeight, leftBearing, charWidth;
};

class FontPrivate;
class Font {
public:
    Font();
    Font(const std::string& fontName, bool bold = false, bool italic = false);
    ~Font();

    void setChar(unsigned char symbol, int texWidth, int texHeight, const char* data, int leftBearing, int width);
    FontSymbol* charData(unsigned char symbol) const;

    void registerFont(const std::string& fontName, bool bold, bool italic, int wordSpacing);

    bool isValid() const;

    float draw(float x, float y, const std::string& text, const GXColor& color);

private:
    FontPrivate* d; 
};

}

#endif
