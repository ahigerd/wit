#include "wit.h"
#include "geometry.h"
#include <vector>

namespace wit {

/**
 * \class Point geometry.h wit/geometry.h
 *
 * The Point class describes a two-dimensional coordinate pair.
 */

/**
 * \var float Point::x
 *
 * The x coordinate of the point.
 */

/**
 * \var float Point::y
 *
 * The y coordinate of the point.
 */

/**
 * Creates a new Point object at (0, 0).
 */
Point::Point() : x(0), y(0)
{
    // initializers only
}

/**
 * Creates a new Point object with the same coordinates as \em other.
 */
Point::Point(const Point& other) : x(other.x), y(other.y)
{
    // initializers only
}

/**
 * Creates a new Point object at (x, y).
 */
Point::Point(float x, float y) : x(x), y(y)
{
    // initializers only
}

/**
 * Copies the coordinates of \em other to this object.
 */
Point& Point::operator=(const Point& other)
{
    x = other.x;
    y = other.y;
    return *this;
}

/**
 * \class Rect geometry.h wit/geometry.h
 *
 * The Rect class describes a rectangle aligned with the horizontal and vertical axes.
 */

/**
 * \var float Rect::x1
 *
 * The x coordinate of the left side of the rectangle.
 */

/**
 * \var float Rect::x2
 *
 * The x coordinate of the right side of the rectangle.
 */

/**
 * \var float Rect::y1
 *
 * The y coordinate of the top side of the rectangle.
 */

/**
 * \var float Rect::y2
 *
 * The y coordinate of the bottom side of the rectangle.
 */

/**
 * Creates a new Rect object at (0, 0) with no size.
 */
Rect::Rect() : x1(0), y1(0), x2(0), y2(0)
{
    // initializers only
}

/**
 * Creates a new Rect object that is a copy of \em other.
 */
Rect::Rect(const Rect& other) : x1(other.x1), y1(other.y1), x2(other.x2), y2(other.y2)
{
    // initializers only
}

/**
 * Creates a new Rect object that spans from (x1, y1) to (x2, y2).
 */
Rect::Rect(float x1, float y1, float x2, float y2) : x1(x1), y1(y1), x2(x2), y2(y2)
{
    // initializers only
}

/**
 * Replaces the coordinates of this rectangle with the coordinates from \em other.
 */
Rect& Rect::operator=(const Rect& other)
{
    x1 = other.x1;
    y1 = other.y1;
    x2 = other.x2;
    y2 = other.y2;
    return *this;
}

/**
 * Returns true if the point (x, y) is contained within the rectangle.
 *
 * Note that a rectangle with non-positive height or width contains no points.
 */
bool Rect::contains(float x, float y)
{
    return (x1 <= x && x2 >= x) && (y1 <= y && y2 >= y);
}

/**
 * Returns true if the rect \em other intersects with this rectangle.
 *
 * Note that a rectangle with non-positive height or width intersects with nothing.
 */
bool Rect::intersects(const Rect& other)
{
    return (x2 >= other.x1 && x1 <= other.x2) && (y2 >= other.y1 && y1 <= other.y2);
}

/**
 * Returns the width of the rectangle.
 *
 * Note that the width may be negative if x2 < x1.
 */
float Rect::width() const
{
    return x2 - x1;
}

/**
 * Returns the height of the rectangle.
 *
 * Note that the height may be negative if y2 < y1.
 */
float Rect::height() const
{
    return y2 - y1;
}

/**
 * \class Polygon geometry.h wit/geometry.h
 *
 * The Polygon class describes an arbitrary two-dimensional polygon.
 */

struct PolygonPrivate {
    std::vector<Point> points;
    Rect bb;
};

/**
 * Creates a new empty Polygon object.
 */
Polygon::Polygon() : d(new PolygonPrivate)
{
    // initializers only
}

/**
 * Creates a new Polygon object that is a copy of \em other.
 */
Polygon::Polygon(const Polygon& other) : d(new PolygonPrivate)
{
    this->operator=(other);
}

/**
 * Destroys the Polygon object.
 */
Polygon::~Polygon()
{
    delete d;
}

/**
 * Replaces the points of the Polygon object with the points from \em other.
 */
Polygon& Polygon::operator=(const Polygon& other)
{
    d->points = other.d->points;
    d->bb = other.d->bb;
    return *this;
}

/**
 * Adds a point to the polygon.
 */
void Polygon::addPoint(const Point& p)
{
    d->points.push_back(p);
    if(d->points.size() == 1) {
        d->bb.x1 = d->bb.x2 = p.x;
        d->bb.y1 = d->bb.y2 = p.y;
    } else {
        if(d->bb.x1 > p.x) d->bb.x1 = p.x;
        if(d->bb.x2 < p.x) d->bb.x2 = p.x;
        if(d->bb.y1 > p.y) d->bb.y1 = p.y;
        if(d->bb.y2 < p.y) d->bb.y2 = p.y;
    }
}

/**
 * Adds a point to the polygon.
 *
 * This is an overloaded member function, provided for convenience.
 */
void Polygon::addPoint(float x, float y)
{
    addPoint(Point(x, y));
}

/**
 * Returns the number of points in the polygon.
 */
int Polygon::count() const
{
    return d->points.size();
}

/**
 * Returns a pointer to the array of points in the polygon.
 *
 * These points may be modified in-place.
 */
Point* Polygon::points() const
{
    return &(d->points.front());
}

/**
 * Returns the axis-aligned bounding box of the polygon.
 *
 * This bounding box is the smallest rectangle parallel to the
 * horizontal and vertical axes contains the entire polygon.
 */
Rect Polygon::boundingBox() const
{
    return d->bb;
}

/**
 * Returns true if the point (x, y) is contained within the polygon.
 *
 * This function uses the odd/even method of determining whether or not
 * the point is contained within the polygon. Therefore, a polygon whose
 * boundary contains self-intersections may not yield the expected results.
 */
bool Polygon::contains(float x, float y) const
{
    bool inside = false;
    Point* p = points();

    for(int i = count() - 2; i >= -1; --i) {
        Point p1 = p[i+1];
        Point p2 = (i == -1 ? p[count() - 1] : p[i]);

        if((p2.x < x) != (x <= p1.x))
            continue;

        if(p1.x < p2.x) {
            p2 = p1;
            p1 = (i == -1 ? p[count() - 1] : p[i]);
        }

        if((y - p1.y) * (p2.x - p1.x) < (p2.y - p1.y) * (x - p1.x))
            inside = !inside;
    }
    return inside;
}

}
