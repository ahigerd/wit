#ifndef WIT_GEOMETRY_H
#define WIT_GEOMETRY_H

namespace wit {

class Point {
public:
    Point();
    Point(const Point& other);
    Point(float x, float y);

    Point& operator=(const Point& other);
    float x, y;
};

class Rect {
public:
    Rect();
    Rect(const Rect& other);
    Rect(float x1, float y1, float x2, float y2);

    Rect& operator=(const Rect& other);
    float x1, y1, x2, y2;

    bool contains(float x, float y);
    bool intersects(const Rect& other);
    float width() const;
    float height() const;
};

class PolygonPrivate;
class Polygon {
public:
    Polygon();
    Polygon(const Polygon& other);
    ~Polygon();
    
    Polygon& operator=(const Polygon& other);

    void addPoint(float x, float y);
    void addPoint(const Point& p);
    int count() const;
    Point* points() const;

    Rect boundingBox() const;

    bool contains(float x, float y) const;

private:
    PolygonPrivate* d;
};

}

#endif
