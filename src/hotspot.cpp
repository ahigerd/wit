#include "hotspot.h"
#include <vector>
#include <algorithm>

namespace wit {

/**
 * \class Hotspot hotspot.h wit/hotspot.h
 *
 * The Hotspot class is used for dispatching \ref PointerEvent events to \ref EventHandler objects.
 *
 * A hotspot is, in essence, a claim laid to a particular region of the screen. When a Wii Remote
 * is pointed at this region, the pointer events generated by it are sent to the assigned
 * eventHandler(). When the Wii Remote is moved out of the region, a \ref PointerLeaveEvent is
 * likewise generated and dispatched to the eventHandler().
 *
 * An EventHandler may push itself onto the focus stack (see Application::pushFocus) in response
 * to the first PointerEvent received and then remove itself (see Application::removeFromFocusStack)
 * in response to receiving a PointerLeaveEvent in order to capture button presses sent while the
 * Wii Remote is pointed at it.
 *
 * \sa EventHandler
 */

static std::vector<Hotspot*> wit_hotspot_list;

struct HotspotPrivate {
    EventHandler* handler;
    Polygon poly;
    bool isRect, enabled;
    int zorder;
};

/**
 * Creates a new Hotspot object. The hotspot's region is initially empty.
 */
Hotspot::Hotspot() : d(new HotspotPrivate)
{
    d->handler = 0;
    d->isRect = true;
    d->enabled = true;
    d->zorder = 0;
    wit_hotspot_list.push_back(this);
}

/**
 * Destroys the Hotspot object.
 */
Hotspot::~Hotspot()
{
    wit_hotspot_list.erase(std::find(wit_hotspot_list.begin(), wit_hotspot_list.end(), this));
    delete d;
}

/**
 * Returns true if the hotspot is enabled.
 * \sa setEnabled()
 */
bool Hotspot::isEnabled() const
{
    return d->enabled;
}

/**
 * Sets whether or not the hotspot is enabled.
 *
 * Hotspots will only be used for dispatching pointer events if
 * they are enabled, non-empty (see region()), and have an
 * event handler set (see eventHandler()).
 * \sa isEnabled()
 */
void Hotspot::setEnabled(bool on)
{
    d->enabled = on;
}

/**
 * Returns the axis-aligned bounding box of the hotspot.
 *
 * This bounding box is the smallest rectangle parallel to the
 * sides of the screen that contains the entire region.
 */
Rect Hotspot::boundingBox() const
{
    return d->poly.boundingBox();
}

/**
 * Returns the region of the hotspot.
 *
 * \sa setRegion()
 */
Polygon Hotspot::region() const
{
    return d->poly;    
}

/**
 * Sets the region of the hotspot to the specified rectangle.
 *
 * Rectangular hotspot regions should be created using this overload,
 * not by passing a rectangular polygon to the other overload, because
 * it enables performance optimizations that cannot be done with
 * arbitrary polygons.
 */
void Hotspot::setRegion(const Rect& rect)
{
    d->isRect = true;
    Polygon poly;
    poly.addPoint(rect.x1, rect.y1);
    poly.addPoint(rect.x1, rect.y2);
    poly.addPoint(rect.x2, rect.y2);
    poly.addPoint(rect.x2, rect.y1);
    d->poly = poly;
}

/**
 * Sets the region of the hotspot to the specified polygon.
 */
void Hotspot::setRegion(const Polygon& polygon)
{
    d->isRect = false;
    d->poly = polygon;
}

/**
 * Returns true if the specified point is contained within the
 * hotspot's bounding box.
 *
 * This operation is significantly faster than contains() for
 * non-rectangular regions.
 *
 * \sa boundingBox()
 */
bool Hotspot::boundingBoxContains(float x, float y)
{
    return boundingBox().contains(x, y);
}

/**
 * Returns true if the specified point is contained within the
 * hotspot's region.
 *
 * \sa region()
 */
bool Hotspot::contains(float x, float y)
{
    bool bb = boundingBox().contains(x, y);
    if(!bb || d->isRect)
        return bb;
    return d->poly.contains(x, y);
}

/**
 * Sets the event handler that will receive pointer events
 * generated when the Wii Remote enters the hotspot's region.
 *
 * \sa eventHandler()
 */
void Hotspot::setEventHandler(EventHandler* handler)
{
    d->handler = handler;
}

/**
 * Sets the event handler that will receive pointer events
 * generated when the Wii Remote enters the hotspot's region.
 *
 * \sa setEventHandler()
 */
EventHandler* Hotspot::eventHandler() const
{
    return d->handler;
}

/**
 * Returns the Z-index of the hotspot.
 *
 * \sa setZIndex()
 */
int Hotspot::zIndex() const
{
    return d->zorder;
}

/**
 * Sets the Z-index of the hotspot.
 *
 * If more than one hotspot lays claim to some portion of the
 * screen, the hotspot with the greatest Z-index will receive
 * the event. If multiple hotspots have the same Z-index, an
 * arbitrarily selected hotspot will receive the event.
 *
 * \sa setZIndex()
 */
void Hotspot::setZIndex(int z)
{
    d->zorder = z;
}

/**
 * Returns a list of all hotspots that contain the specified point.
 */
std::auto_ptr<HotspotList> Hotspot::findAtPoint(float x, float y)
{
    std::auto_ptr<HotspotList> rv(new HotspotList);
    if(!wit_hotspot_list.size()) return rv;

    Hotspot** end = &wit_hotspot_list.back();
    Hotspot* hs;
    for(Hotspot** iter = &wit_hotspot_list.front(); iter <= end; iter++) {
        if(!iter) continue;
        hs = *iter;
        if(!hs->isEnabled())
            continue;
        if(hs->contains(x, y))
            rv->push_back(hs);
    }
    return rv;
}

}
