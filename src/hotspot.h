#ifndef WIT_HOTSPOT_H
#define WIT_HOTSPOT_H

#include "geometry.h"
#include <vector>
#include <memory>

namespace wit {

class EventHandler;
class HotspotPrivate;
class Hotspot;
typedef std::vector<Hotspot*> HotspotList;

class Hotspot {
public:
    Hotspot();
    virtual ~Hotspot();

    bool isEnabled() const;
    void setEnabled(bool on);

    Rect boundingBox() const;
    bool boundingBoxContains(float x, float y);

    Polygon region() const;
    void setRegion(const Rect& rect);
    void setRegion(const Polygon& polygon);
    bool contains(float x, float y);

    void setEventHandler(EventHandler* handler);
    EventHandler* eventHandler() const;

    int zIndex() const;
    void setZIndex(int z);

    static std::auto_ptr<HotspotList> findAtPoint(float x, float y);
    
private:
    friend class HotspotPrivate;
    HotspotPrivate* d;
};

}

#endif
