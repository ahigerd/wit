#include "renderer.h"
#include "application.h"
#include <gctypes.h>
#include <ogc/gx_struct.h>

namespace wit {

/** \class Renderer renderer.h wit/renderer.h
 * The Renderer class provides an interface for objects that wish to render graphics
 * on the screen.
 *
 * The render() function will be called on each Renderer object every vblank; if
 * double-buffering is enabled, this will be immediately before the framebuffer is
 * flipped.
 *
 * The Renderer class also provides a number of convenience functions for drawing
 * graphical primitives. You are not required to use these functions; you may use
 * direct framebuffer access, the text console (if wit::UseConsole is enabled),
 * GX (if wit::UseGX is enabled), or third-party graphics libraries for rendering.
 */

// doubly-linked list for faster deletes
struct RendererPrivate {
    RendererPrivate* prev;
    Renderer* renderer;
    float scale, transx, transy; 
    RendererPrivate* next;
};

static RendererPrivate* wit_render_list = 0;

/** \private */
void wit_render_all()
{
    RendererPrivate* node = wit_render_list;
    while(node) {
        node->renderer->render();
        node = node->next;
    }
    node = wit_render_list;
    while(node) {
        node->renderer->waitForRendering();
        node = node->next;
    }
}

/**
 * Constructs a new Renderer.
 */
Renderer::Renderer()
{
    d = new RendererPrivate;
    d->prev = 0;
    d->renderer = this;
    d->scale = 1.0;
    d->transx = d->transy = 0.0;
    d->next = wit_render_list;
    if(wit_render_list)
        wit_render_list->prev = d;
    wit_render_list = d;
}

/**
 * Destroys the Renderer.
 */
Renderer::~Renderer()
{
    if(d->prev) 
        d->prev->next = d->next;
    else
        wit_render_list = d->next;
    if(d->next)
        d->next->prev = d->prev;
    delete d;
}

/**
 * \fn virtual void Renderer::render() = 0
 * Implement this function to perform whatever steps are necessary to draw the
 * Renderer's content on the screen.
 *
 * To render in another thread, you may use this function to signal a rendering
 * thread to begin processing, or you may ignore this function and use
 * waitForRendered() to control the event loop when rendering is complete.
 */

/**
 * If you are performing rendering in another thread, implement this function
 * to block the main event loop until the rendering is complete.
 *
 * The default implementation does nothing.
 */
void Renderer::waitForRendering()
{
    // do nothing
}

static void wit_renderer_transform(RendererPrivate* d)
{
    static int width = wit::Application::instance()->screenWidth() / 2;

    Mtx44 perspective;
    float xmin = width + d->transx - (width / d->scale);
    float xmax = width + d->transx + (width / d->scale) - 1;
    float ymin = 240 + d->transy - (240 / d->scale);
    float ymax = 240 + d->transy + (240 / d->scale) - 1;
    guOrtho(perspective, ymin, ymax, xmin, xmax, 0, 300);
    GX_LoadProjectionMtx(perspective, GX_ORTHOGRAPHIC);
}

static void wit_renderer_setup(RendererPrivate* d)
{
    wit_renderer_transform(d);

    GX_SetNumChans(1);
    GX_SetNumTexGens(0);
    GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);

    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_TEX0, GX_NONE);
}

/**
 * Returns the current scaling factor applied to the convenience functions for this renderer object.
 *
 * \sa setScale()
 */
float Renderer::scale() const
{
    return d->scale;
}

/**
 * Sets the scaling factor applied to the convenience functions for this renderer object.
 *
 * The default value of 1.0 performs no scaling.
 *
 * \sa scale()
 */
void Renderer::setScale(float mag)
{
    d->scale = mag;
}

/**
 * Returns the horizontal translation applied to the convenience functions for this renderer object.
 *
 * \sa setTranslation()
 */
float Renderer::translateX() const 
{
    return d->transx;
}

/**
 * Returns the vertical translation applied to the convenience functions for this renderer object.
 *
 * \sa setTranslation()
 */
float Renderer::translateY() const
{
    return d->transy;
}

/**
 * Sets the translation applied to the convenience functions for this renderer object.
 *
 * The default value of 0.0 for both horizontal and vertical translations performs no translation.
 *
 * \sa translateX(), translateY()
 */
void Renderer::setTranslation(float x, float y)
{
    d->transx = x;
    d->transy = y;
}

/**
 * Draws a point on the screen in the selected color.
 */
void Renderer::drawPoint(float x, float y, const GXColor& color)
{
    wit_renderer_setup(d);

    GX_Begin(GX_POINTS, GX_VTXFMT7, 1);
    GX_Position2f32(x, y);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

/**
 * Draws a point on the screen in the selected color.
 *
 * This is an overloaded member function provided for convenience.
 */
void Renderer::drawPoint(const Point& p, const GXColor& color)
{
    drawPoint(p.x, p.y, color);
}

/**
 * Draws a line on the screen in the selected color.
 */
void Renderer::drawLine(float x1, float y1, float x2, float y2, const GXColor& color)
{
    wit_renderer_setup(d);

    GX_Begin(GX_LINES, GX_VTXFMT7, 2);
    GX_Position2f32(x1, y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(x2, y2);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

/**
 * Draws a line on the screen in the selected color.
 *
 * This is an overloaded member function provided for convenience.
 */
void Renderer::drawLine(const Point& p1, const Point& p2, const GXColor& color)
{
    drawLine(p1.x, p1.y, p2.x, p2.y, color);
}

/**
 * Draws a rectangle on the screen in the specified color.
 */
void Renderer::drawRect(const Rect& rect, const GXColor& color)
{
    wit_renderer_setup(d);

    GX_Begin(GX_LINESTRIP, GX_VTXFMT7, 5);
    GX_Position2f32(rect.x1, rect.y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x2, rect.y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x2, rect.y2);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x1, rect.y2);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x1, rect.y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

/**
 * Draws a rectangle on the screen and fills it with the specified color.
 */
void Renderer::fillRect(const Rect& rect, const GXColor& color)
{
    wit_renderer_setup(d);

    GX_Begin(GX_QUADS, GX_VTXFMT7, 4);
    GX_Position2f32(rect.x1, rect.y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x2, rect.y1);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x2, rect.y2);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_Position2f32(rect.x1, rect.y2);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

/**
 * Draws a series of points on the screen in the specified color.
 */
void Renderer::drawPoints(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();

    GX_Begin(GX_POINTS, GX_VTXFMT7, ct);
    for(int i = 0; i < ct; i++) {
        GX_Position2f32(points[i].x, points[i].y);
        GX_Color4u8(color.r, color.g, color.b, color.a);
    }
    GX_End();
}

/**
 * Draws a polygon on the screen in the specified color.
 */
void Renderer::drawPolygon(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();

    GX_Begin(GX_LINESTRIP, GX_VTXFMT7, ct+1);
    for(int i = 0; i < ct; i++) {
        GX_Position2f32(points[i].x, points[i].y);
        GX_Color4u8(color.r, color.g, color.b, color.a);
    }
    GX_Position2f32(points[0].x, points[0].y);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

/**
 * Draws a polygon on the screen and fills it with the specified color.
 *
 * Note that this function will only accurately fill convex polygons whose
 * points are specified in clockwise order.
 */
void Renderer::fillPolygon(const Polygon& poly, const GXColor& color)
{
    wit_renderer_setup(d);

    int ct = poly.count();
    Point* points = poly.points();

    GX_Begin(GX_TRIANGLEFAN, GX_VTXFMT7, ct+1);
    for(int i = 0; i < ct; i++) {
        GX_Position2f32(points[i].x, points[i].y);
        GX_Color4u8(color.r, color.g, color.b, color.a);
    }
    GX_Position2f32(points[0].x, points[0].y);
    GX_Color4u8(color.r, color.g, color.b, color.a);
    GX_End();
}

}
