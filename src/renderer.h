#ifndef WIT_RENDERER_H
#define WIT_RENDERER_H

#include "geometry.h"
#include "wit.h"

namespace wit {

class RendererPrivate;
class Renderer {
public:
    Renderer();
    virtual ~Renderer();

    virtual void render() = 0;
    virtual void waitForRendering();

    float scale() const;
    void setScale(float mag);

    float translateX() const;
    float translateY() const;
    void setTranslation(float x, float y);

    void drawPoint(const Point& p, const GXColor& color);
    void drawPoint(float x, float y, const GXColor& color);

    void drawLine(const Point& p1, const Point& p2, const GXColor& color);
    void drawLine(float x1, float y1, float x2, float y2, const GXColor& color);

    void drawRect(const Rect& rect, const GXColor& color);
    void fillRect(const Rect& rect, const GXColor& color);

    void drawPoints(const Polygon& poly, const GXColor& color);
    void drawPolygon(const Polygon& poly, const GXColor& color);
    void fillPolygon(const Polygon& poly, const GXColor& color);

private:
    RendererPrivate* d;
};

}

#endif
