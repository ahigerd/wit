#include "widget.h"
#include "event.h"
#include "application.h"

namespace wit {

/**
 * \class Widget widget.h wit/widget.h
 *
 * The Widget class provides an event-sensitive region on the screen that can be drawn upon.
 *
 * Unlike most desktop-style interface toolkits, the Widget class is not the base class for
 * all interface objects; rather, it is a convenience class combining \ref EventHandler,
 * \ref Hotspot, and \ref Renderer in a single object. Its primary use is for implementing
 * buttons and button-like objects through its auto-focus feature, which allows the widget
 * to accept events (such as button events) while the Wii Remote is pointed at it.
 *
 * For interface elements that do not need to accept events, use \ref Renderer alone. For
 * interface elements that do not need to accept pointer events, inherit both \ref Renderer
 * and \ref EventHandler. To receive all pointer events, regardless of screen position,
 * inherit from \ref EventHandler and use Application::installEventFilter().
 */

struct WidgetPrivate {
    unsigned char hasFocus : 4;
    bool autoFocus : 1;
};

/**
 * Constructs a new Widget object.
 */
Widget::Widget() : EventHandler(), Hotspot(), Renderer()
{
    d = new WidgetPrivate;
    d->autoFocus = true;
    d->hasFocus = 0;
    setEventHandler(this);
}

/**
 * Returns true if the widget is set to auto-focus.
 *
 * \sa setAutoFocus()
 */
bool Widget::autoFocus() const
{
    return d->autoFocus;
}

/**
 * Enables or disables the auto-focus feature.
 *
 * When auto-focus is enabled (the default), pointing the Wii Remote at the widget's
 * hotspot (see region()) pushes the widget onto that controller's focus stack and
 * moving the pointer away from the hotspot removes it from that stack.
 *
 * \sa autoFocus()
 */
void Widget::setAutoFocus(bool on)
{
    d->autoFocus = on;
}

/**
 * Returns true if the specified Wii Remote is currently pointing at the widget.
 *
 * Passing -1 as the controllerID (the default) will return true if any Wii Remotes
 * are currently pointing at the widget.
 */
bool Widget::hasPointer(int controllerID) const
{
    if(controllerID == -1)
        return d->hasFocus;
    return d->hasFocus & (1 << controllerID);
}

/**
 * Processes a pointer event.
 *
 * If autoFocus() is enabled, the default behavior captures the focus, invokes focusEvent(),
 * and returns true. If autoFocus() is disabled, the default behavior simply returns true.
 */
bool Widget::pointerEvent(PointerEvent* event)
{
    unsigned char bit = (1 << event->controllerNumber);
    if(d->autoFocus && !(d->hasFocus & bit)) {
        d->hasFocus |= bit;
        Application::instance()->pushFocus(this, event->controllerNumber);
        focusEvent(event);
    }
    return true;
}

/**
 * Notifies the widget that it has gained the focus via auto-focus.
 *
 * Note that this is not a real event handler; that is, it is not dispatched via event() and
 * event filters on \ref Application will not receive it. The event object passed to focusEvent()
 * is the PointerEvent that caused the focus to be gained.
 */
void Widget::focusEvent(PointerEvent* event)
{
    // do nothing
    (void)event;
}

/**
 * Processes a pointer leave event.
 *
 * The default implementation relinquishes the focus if autoFocus() is enabled and
 * returns true.
 */
bool Widget::pointerLeaveEvent(PointerLeaveEvent* event)
{
    unsigned char bit = (1 << event->controllerNumber);
    if(d->autoFocus && (d->hasFocus & bit)) {
        d->hasFocus &= ~bit;
        Application::instance()->removeFromFocusStack(this, event->controllerNumber);
    }
    return true;
}

}
