#ifndef WIT_WIDGET_H
#define WIT_WIDGET_H

#include "eventhandler.h"
#include "hotspot.h"
#include "renderer.h"

namespace wit {

class PointerEvent;
class PointerLeaveEvent;
class WidgetPrivate;

class Widget : public EventHandler, public Hotspot, public Renderer
{
public:
    Widget();

    bool autoFocus() const;
    void setAutoFocus(bool on);
    bool hasPointer(int controllerID = -1) const;

    virtual void focusEvent(PointerEvent* event);
    virtual bool pointerEvent(PointerEvent* event);
    virtual bool pointerLeaveEvent(PointerLeaveEvent* event);

private:
    WidgetPrivate* d;
};

}

#endif
