#include "wit.h"

// Currently this file is only used to hold generic documentation.
// Global non-inline functions may live here someday.

/**
 * \mainpage Wii Interface Toolkit
 * 
 * WIT, the Wii Interface Toolkit, is an event-driven C++-based development toolkit intended for Wii software development.
 *
 * The intent of WIT is to encapsulate most of the mundane details of the Wii platform behind a consistent programming
 * interface while remaining relatively lightweight. The Wii Remote and Nunchuk, the Classic Controller, and GameCube
 * controllers are all managed through the same event system, allowing all forms of input to be handled consistently
 * without requiring the developer to write separate code for each controller type. The Guitar Hero 3 and Balance Board
 * accessories are also supported. WIT's design is inclusive, allowing the developer to use native API calls and third-party
 * libraries without restriction or interference.
 *
 * A secondary goal is to be the most thoroughly documented homebrew library available. Every constant, function, and
 * class exposed by the WIT API is fully documented.
 *
 * The current implementation of WIT is based on <a href='http://libogc.devkitpro.org/'>libOGC</a>, but the toolkit API is
 * backend-independent and should be readily portable to the official WiiWare SDK; likewise, a desktop-based Wii simulator
 * using native graphics and input backends (perhaps through <a href='http://www.wiiuse.net'>wiiuse</a> or simulating the
 * Wii Remote with a mouse) would be possible.
 *
 * Example source code is available in the WIT source distribution.
 */
