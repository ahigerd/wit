#include "fontitem.h"
#include <QtDebug>
#include <QPixmap>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QFontComboBox>
#include <QSpinBox>
#include <QTextEdit>
#include <QCheckBox>
#include <QByteArray>
#include <QFont>
#include <QFontMetrics>
#include <QFile>
#include <QRegExp>
#include <QPainter>
#include <QFileDialog>

static QByteArray convertTexture(const QImage& image)
{
    // converts a QImage into IA4 format

    int w = image.width(), h = image.height(), p = 0;
    QByteArray rv(w * h, '\0'); // target format is 8bpp

    // target format uses 8x4 tiles
    for(int y = 0; y < h; y += 4) {
        for(int x = 0; x < w; x += 8) {
            for(int ys = y; ys < y + 4; ys++) {
                for(int xs = x; xs < x + 8; xs++) {
                    // pixel() returns 0xAARRGGBB
                    int px = image.pixel(xs, ys);
                    if(px & 0xF0000000)
                        rv[p++] = (px & 0xF0000000 >> 24) | 0xF; // (px >> 28); 
                    else
                        rv[p++] = 0x0F;
                }
            }
        }
    }
   
    return rv; 
}

FontItem::FontItem(QWidget* parent) : QWidget(parent)
{
    setWindowTitle("font2wit");

    QGridLayout* layout = new QGridLayout(this);

    layout->addWidget(new QLabel("Name:", this), 0, 0);
    fontName = new QLineEdit(this);
    layout->addWidget(fontName, 0, 1, 1, 3);

    layout->addWidget(new QLabel("Font:", this), 1, 0);
    fontCombo = new QFontComboBox(this);
    layout->addWidget(fontCombo, 1, 1, 1, 3);

    layout->addWidget(new QLabel("Size:", this), 2, 0);
    fontSize = new QSpinBox(this);
    fontSize->setRange(4, 72);
    fontSize->setValue(12);
    layout->addWidget(fontSize, 2, 1);

    genNormal = new QCheckBox("Normal", this);
    genBold = new QCheckBox("Bold", this);
    genItalic = new QCheckBox("Italic", this);
    genBoldItalic = new QCheckBox("Bold Italic", this);
    genNormal->setChecked(true);
    layout->addWidget(genNormal, 2, 2);
    layout->addWidget(genBold, 2, 3);
    layout->addWidget(genItalic, 3, 2);
    layout->addWidget(genBoldItalic, 3, 3);

    QPushButton* genButton = new QPushButton("Generate", this);
    layout->addWidget(genButton, 3, 0, 1, 2);

    QObject::connect(genButton, SIGNAL(clicked()), this, SLOT(generate()));
    QObject::connect(fontCombo, SIGNAL(currentFontChanged(QFont)), this, SLOT(fontChanged()));
    QObject::connect(fontSize, SIGNAL(valueChanged(int)), this, SLOT(fontChanged()));

    fontChanged();
}

void FontItem::fontChanged()
{
    QByteArray basename = fontCombo->currentFont().family().replace(QRegExp("[^A-Za-z]"), "").toAscii()+QByteArray::number(fontSize->value());
    fontName->setText(basename);
}

void FontItem::generate()
{
    QFont f(fontCombo->currentFont());
    f.setPixelSize(fontSize->value());

    QByteArray basename = fontName->text().toAscii();
    QString filename = QFileDialog::getSaveFileName(this, "Save As...", basename+".cpp", QString("C++ Files (*.cpp)"));
    if(filename.isEmpty()) return;

    QFile ff(filename);
    ff.open(QIODevice::WriteOnly);
    ff.write("#include <wit/font.h>\n\n");
    if(genNormal->isChecked()) {
        f.setBold(false);
        f.setItalic(false);
        generate(basename, f, ff);
    }
    if(genBold->isChecked()) {
        f.setBold(true);
        f.setItalic(false);
        generate(basename, f, ff);
    }
    if(genItalic->isChecked()) {
        f.setBold(false);
        f.setItalic(true);
        generate(basename, f, ff);
    }
    if(genBoldItalic->isChecked()) {
        f.setBold(true);
        f.setItalic(true);
        generate(basename, f, ff);
    }
    ff.close();
}

static int getRealBoundingWidth(const QImage& img)
{
    int width = img.width();
    int height = img.height();
    bool leadingColumns = true;
    int leadingCount = 0;
    for(int x = 0; x < width; x++) {
        bool columnIsBlank = true;
        for(int y = 0; y < height; y++) {
            if(img.pixel(x, y) & 0xF0000000) {
                columnIsBlank = false;
                break;
            }
        }
        if(columnIsBlank) {
            if(leadingColumns)
                leadingCount++;
            else
                return x - leadingCount;
        } else {
            leadingColumns = false;
        }
    }
    return width;
}

void FontItem::generate(const QByteArray& basename, const QFont& font, QFile& ff)
{
    QFontMetrics fm(font); 
    int baseline = fm.ascent();
    QByteArray structName = basename;
    if(font.bold()) structName += "_B";
    if(font.italic()) structName += "_I";
    ff.write("static struct "+structName+"_Registrar { "+structName+"_Registrar(); } reg_"+structName+";\n\n");
    ff.write(structName+"_Registrar::"+structName+"_Registrar() {\n    wit::Font* f = new wit::Font;\n");
    for(char i = 33; i < 127; i++) {
        QString letter = QChar(i);
        QRect bb = fm.boundingRect(QChar(i));
        QSize size(bb.width(), fm.height());
        QSize osize = size;
        size.setWidth((size.width() | 0x7) + 1);
        size.setHeight((size.height() | 0x3) + 1);
        QImage img(size, QImage::Format_ARGB32);
        int lb = fm.leftBearing(i);
        img.fill(qRgba(0, 0, 0, 0));
        QPainter p(&img);
        p.setPen(Qt::white);
        p.drawText(-bb.left(), baseline, letter);
        p.end();
        int realWidth = getRealBoundingWidth(img);
        QByteArray data = convertTexture(img);
        ff.write("    f->setChar("+QByteArray::number(i)+", "+QByteArray::number(size.width())+", "+QByteArray::number(size.height())+", (char[]){ ");
        for(int j = 0; j < data.size(); j++) {
            ff.write(QByteArray::number((unsigned char)data[j])+", ");
        }
        ff.write("0 }, "+QByteArray::number(lb)+", "+QByteArray::number(realWidth + fm.rightBearing(i) /*fm.width(i)*/)+");\n");
        ff.flush();
        qDebug() << letter << osize << size << lb << realWidth << fm.width(i) << fm.rightBearing(i);
    }
    QByteArray b = font.bold() ? "true" : "false";
    QByteArray i = font.italic() ? "true" : "false";
    ff.write("    f->registerFont(\""+basename+"\", "+b+", "+i+", "+QByteArray::number(fm.width(" "))+");\n}\n\n");
}

