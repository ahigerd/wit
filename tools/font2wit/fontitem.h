#ifndef FONTITEM_H
#define FONTITEM_H

#include <QWidget>
class QFontComboBox;
class QSpinBox;
class QCheckBox;
class QLineEdit;
class QFile;

class FontItem : public QWidget {
Q_OBJECT
public:
    FontItem(QWidget* parent = 0);

public slots:
    void generate();

private slots:
    void fontChanged();

private:
    void generate(const QByteArray& basename, const QFont& font, QFile& ff);

    QFontComboBox *fontCombo;
    QSpinBox *fontSize;
    QCheckBox *genNormal, *genBold, *genItalic, *genBoldItalic;
    QLineEdit *fontName;
};

#endif
