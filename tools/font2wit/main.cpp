#include <QApplication>
#include "fontitem.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    FontItem i;
    i.show();

    return app.exec();
}
